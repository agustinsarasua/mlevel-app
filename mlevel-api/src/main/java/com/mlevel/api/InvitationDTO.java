package com.mlevel.api;

import com.mlevel.api.authentication.UserDTO;

import java.util.Date;

/**
 * Created by agustin on 17/03/16.
 */
public class InvitationDTO extends AbstractDTO {

    private String uuid;

    private UserDTO userDTO;

    private Date invitationDate;

    private ActionDTO actionDTO;

    private Date invitationDueDate;

    private EventDTO eventDTO;

    private String state;

    private Date lastUpdate;

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ActionDTO getActionDTO() {
        return actionDTO;
    }

    public void setActionDTO(ActionDTO actionDTO) {
        this.actionDTO = actionDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public Date getInvitationDate() {
        return invitationDate;
    }

    public void setInvitationDate(Date invitationDate) {
        this.invitationDate = invitationDate;
    }

    public Date getInvitationDueDate() {
        return invitationDueDate;
    }

    public void setInvitationDueDate(Date invitationDueDate) {
        this.invitationDueDate = invitationDueDate;
    }

    public EventDTO getEventDTO() {
        return eventDTO;
    }

    public void setEventDTO(EventDTO eventDTO) {
        this.eventDTO = eventDTO;
    }
}
