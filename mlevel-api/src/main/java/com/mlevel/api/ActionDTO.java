package com.mlevel.api;

/**
 * Created by agustin on 17/03/16.
 */
public class ActionDTO extends AbstractDTO {

    private String name;

    private String description;

    public ActionDTO(){}

    public ActionDTO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
