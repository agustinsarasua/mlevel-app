package com.mlevel.api.timeline;

import com.mlevel.api.AbstractDTO;
import com.mlevel.api.location.PlaceDTO;

import java.util.Date;

/**
 * Created by agustin on 02/04/16.
 */
public class TimelineItemDTO extends AbstractDTO {

    public enum TimelineItemType {
        EVENT, OPEN_INVITATION
    }

    private  String uuid;

    private String name;

    private String subtitle;

    private String description;

    private PlaceDTO place;

    private Date startTime;

    private Date endTime;

    private Date creationDate;

    private TimelineItemType timelineItemType;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PlaceDTO getPlace() {
        return place;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setPlace(PlaceDTO place) {
        this.place = place;
    }

    public TimelineItemType getTimelineItemType() {
        return timelineItemType;
    }

    public void setTimelineItemType(TimelineItemType timelineItemType) {
        this.timelineItemType = timelineItemType;
    }
}
