package com.mlevel.api;

import com.mlevel.api.location.LocationDTO;
import com.mlevel.api.timeline.TimelineItemDTO;

import java.util.Date;
import java.util.List;

/**
 * Created by agustin on 16/03/16.
 */
public class EventDTO extends TimelineItemDTO {

    private String imageUrl;

    private String type;

    private List<ActionDTO> commonActions;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ActionDTO> getCommonActions() {
        return commonActions;
    }

    public void setCommonActions(List<ActionDTO> commonActions) {
        this.commonActions = commonActions;
    }
}
