package com.mlevel.api.authentication;

/**
 * Created by agustin on 06/03/16.
 */
public class AuthenticatedUserTokenDTO {

    private String userId;
    private String token;

    public AuthenticatedUserTokenDTO(){}

    public AuthenticatedUserTokenDTO(String userId, String sessionToken) {
        this.userId = userId;
        this.token = sessionToken;
    }

    public String getUserId() {
        return userId;
    }

    public String getToken() {
        return token;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
