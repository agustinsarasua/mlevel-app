import { Injectable } from 'angular2/core';
import {User} from "../model/user.model";
import {USER} from "../mocks/user.mock";

@Injectable()
export class UserService {
    getUser() {
        return Promise.resolve(USER);
    }

    // See the "Take it slow" appendix
    getUserSlowly() {
        return new Promise<User>(resolve =>
            setTimeout(()=>resolve(USER), 2000) // 2 seconds
        );
    }
}