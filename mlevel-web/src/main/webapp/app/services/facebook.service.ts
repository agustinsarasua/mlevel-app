import {Injectable, OnInit} from 'angular2/core';
import {FacebookEvent} from "../model/event.model";
declare var FB: any;

@Injectable()
export class FacebookService implements OnInit {

    ngOnInit():any {
        FB.init({
            appId      : '1709613959262846', // Facebook App ID
            status     : true,  // check Facebook Login status
            cookie     : true,  // enable cookies to allow Parse to access the session
            xfbml      : true,  // initialize Facebook social plugins on the page
            version    : 'v2.6' // point to the latest Facebook Graph API version
        });
    }

    constructor() {
    }

    public loadFacebookEvents(userId: String) {
        return new Promise<FacebookEvent[]>(resolve =>
            FB.api('/me/events', {
                  access_token: localStorage.getItem('token')
            }, (response) => {
                    let result: Array<FacebookEvent> = [];
                    if (response && !response.error) {
                        /* handle the result */
                        response.data.forEach((event) => {
                            let fbEvent = new FacebookEvent(event);
                            result.push(fbEvent);
                        });
                        resolve(result);
                    }
                }));
    }

    public loadEventCover(eventId: String){
        FB.api('/' + eventId, {
            access_token: localStorage.getItem('token'),
            fields: 'cover'
        }, (response) => {
            if (response && !response.error) {
                /* handle the result */
                return response;
            }
        });
    }
}