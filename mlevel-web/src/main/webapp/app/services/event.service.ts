import { Injectable } from 'angular2/core';
import {EVENTS} from "../mocks/event.mock";

@Injectable()
export class EventService {

    getEvents() {
        return Promise.resolve(EVENTS);
    }

    // See the "Take it slow" appendix
    getEventsSlowly() {
        return new Promise<Event>(resolve =>
            setTimeout(()=>resolve(EVENTS), 2000) // 2 seconds
        );
    }

    getEvent(id: string) {
        return Promise.resolve(EVENTS).then(
            events => events.filter(event => event.id === id)[0]
        );
    }
}