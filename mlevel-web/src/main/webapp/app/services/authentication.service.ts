import { Injectable } from 'angular2/core';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class AuthenticationService {
    token: string;

    constructor() {
        this.token = localStorage.getItem('token');
    }

    facebookLogin(accessToken: String){
        /*
         * If we had a login api, we would have done something like this

         return this.http.post('/auth/login', JSON.stringify({
         username: username,
         password: password
         }), {
         headers: new Headers({
         'Content-Type': 'application/json'
         })
         })
         .map((res : any) => {
         let data = res.json();
         this.token = data.token;
         localStorage.setItem('token', this.token);
         });

         for the purpose of this cookbook, we will juste simulate that
         */

        if (accessToken != null) {
            this.token = accessToken;
            localStorage.setItem('token', this.token);
            return Observable.of('token');
        }
    }
}