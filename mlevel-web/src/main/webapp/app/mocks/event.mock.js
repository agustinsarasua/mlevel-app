System.register(["../model/event.model"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var event_model_1;
    var EVENTS;
    return {
        setters:[
            function (event_model_1_1) {
                event_model_1 = event_model_1_1;
            }],
        execute: function() {
            exports_1("EVENTS", EVENTS = [
                new event_model_1.Event("1", "Movie", "El planeta de los simios"),
                new event_model_1.Event("2", "Movie1", "El planeta de los simios"),
                new event_model_1.Event("3", "Movie2", "El planeta de los simios"),
                new event_model_1.Event("4", "Movie3", "El planeta de los simios")
            ]);
        }
    }
});
//# sourceMappingURL=event.mock.js.map