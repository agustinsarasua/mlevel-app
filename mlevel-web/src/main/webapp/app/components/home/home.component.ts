import { Component, OnInit } from 'angular2/core';
import { Router } from 'angular2/router';
import {Alert, DATEPICKER_DIRECTIVES, ACCORDION_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';
import {MDL} from "../common/MaterialDesignLiteUpgradeElement";

@Component({
    selector: 'm-home',
    directives: [Alert, DATEPICKER_DIRECTIVES, ACCORDION_DIRECTIVES, MDL],
    templateUrl: 'app/components/home/home.component.html',
    styleUrls: ['app/components/home/home.component.css']
})
export class HomeComponent implements OnInit {

    constructor(private _router: Router) {
    }

    public dt:Date = new Date();
    private minDate:Date = null;
    private events:Array<any>;
    private tomorrow:Date;
    private afterTomorrow:Date;
    private formats:Array<string> = ['DD-MM-YYYY', 'YYYY/MM/DD', 'DD.MM.YYYY', 'shortDate'];
    private format = this.formats[0];
    private dateOptions:any = {
        formatYear: 'YY',
        startingDay: 1
    };
    private opened:boolean = false;

    public getDate():number {
        return this.dt && this.dt.getTime() || new Date().getTime();
    }

    ngOnInit() {
    }

    public oneAtATime:boolean = true;
    public items:Array<string> = ['Item 1', 'Item 2', 'Item 3'];

    public status:Object = {
        isFirstOpen: true,
        isFirstDisabled: false
    };

    public groups:Array<any> = [
        {
            title: 'Dynamic Group Header - 1',
            content: 'Dynamic Group Body - 1'
        },
        {
            title: 'Dynamic Group Header - 2',
            content: 'Dynamic Group Body - 2'
        }
    ];

    public addItem():void {
        this.items.push(`Items ${this.items.length + 1}`);
    }

}