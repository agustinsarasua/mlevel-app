System.register(['angular2/core', 'angular2/router', 'ng2-bootstrap/ng2-bootstrap', "../common/MaterialDesignLiteUpgradeElement"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, ng2_bootstrap_1, MaterialDesignLiteUpgradeElement_1;
    var HomeComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (ng2_bootstrap_1_1) {
                ng2_bootstrap_1 = ng2_bootstrap_1_1;
            },
            function (MaterialDesignLiteUpgradeElement_1_1) {
                MaterialDesignLiteUpgradeElement_1 = MaterialDesignLiteUpgradeElement_1_1;
            }],
        execute: function() {
            HomeComponent = (function () {
                function HomeComponent(_router) {
                    this._router = _router;
                    this.dt = new Date();
                    this.minDate = null;
                    this.formats = ['DD-MM-YYYY', 'YYYY/MM/DD', 'DD.MM.YYYY', 'shortDate'];
                    this.format = this.formats[0];
                    this.dateOptions = {
                        formatYear: 'YY',
                        startingDay: 1
                    };
                    this.opened = false;
                    this.oneAtATime = true;
                    this.items = ['Item 1', 'Item 2', 'Item 3'];
                    this.status = {
                        isFirstOpen: true,
                        isFirstDisabled: false
                    };
                    this.groups = [
                        {
                            title: 'Dynamic Group Header - 1',
                            content: 'Dynamic Group Body - 1'
                        },
                        {
                            title: 'Dynamic Group Header - 2',
                            content: 'Dynamic Group Body - 2'
                        }
                    ];
                }
                HomeComponent.prototype.getDate = function () {
                    return this.dt && this.dt.getTime() || new Date().getTime();
                };
                HomeComponent.prototype.ngOnInit = function () {
                };
                HomeComponent.prototype.addItem = function () {
                    this.items.push("Items " + (this.items.length + 1));
                };
                HomeComponent = __decorate([
                    core_1.Component({
                        selector: 'm-home',
                        directives: [ng2_bootstrap_1.Alert, ng2_bootstrap_1.DATEPICKER_DIRECTIVES, ng2_bootstrap_1.ACCORDION_DIRECTIVES, MaterialDesignLiteUpgradeElement_1.MDL],
                        templateUrl: 'app/components/home/home.component.html',
                        styleUrls: ['app/components/home/home.component.css']
                    }), 
                    __metadata('design:paramtypes', [router_1.Router])
                ], HomeComponent);
                return HomeComponent;
            }());
            exports_1("HomeComponent", HomeComponent);
        }
    }
});
//# sourceMappingURL=home.component.js.map