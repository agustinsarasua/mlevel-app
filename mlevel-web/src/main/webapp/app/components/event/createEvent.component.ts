import { Component, OnInit } from 'angular2/core';
import {FORM_DIRECTIVES} from "angular2/common";
import {MDL} from "../common/MaterialDesignLiteUpgradeElement";
import {FacebookService} from "../../services/facebook.service";
import {FacebookEvent} from "../../model/event.model";

@Component({
    selector: 'm-create-event',
    templateUrl: 'app/components/event/createEvent.component.html',
    styleUrls: ['app/components/event/createEvent.component.css'],
    directives: [FORM_DIRECTIVES, MDL]
})
export class CreateEventComponent implements OnInit {

    fbEvents: FacebookEvent[] = [];
    
    constructor(private _fbService: FacebookService){}

    ngOnInit() {
        this.loadFacebookEvents()
    }

    loadFacebookEvents(){
        var userID = localStorage.getItem('facebookUserId');

        this._fbService.loadFacebookEvents(userID)
            .then(events => this.fbEvents = events);

    }
}