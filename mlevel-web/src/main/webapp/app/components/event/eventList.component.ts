import {Component, OnInit, Input} from 'angular2/core';
import {Event} from "../../model/event.model";
import {EventService} from "../../services/event.service";
import {EventComponent} from "./event.component";
import {isLoggedin} from "../../util/is-loggedin";
import {CanActivate, Router} from "angular2/router";
import {AuthenticationService} from "../../services/authentication.service";

@Component({
    selector: 'm-event-list',
    templateUrl: 'app/components/event/eventList.component.html',
    styleUrls: ['app/components/event/eventList.component.css'],
    directives:[EventComponent]
})

@CanActivate(() => isLoggedin())
export class EventListComponent implements OnInit {

    events: Event[] = [];

    constructor(private _eventService: EventService, public auth: AuthenticationService, public router: Router) {}

    ngOnInit() {
        this.loadEvents();
    }

    loadEvents() {
        this._eventService.getEvents()
            .then(events => this.events = events);
    }
}