import {Component, OnInit, Input} from 'angular2/core';
import { Router } from 'angular2/router';
import {Event} from "../../model/event.model";
import {EventService} from "../../services/event.service";

@Component({
    selector: 'm-event',
    templateUrl: 'app/components/event/event.component.html',
    styleUrls: ['app/components/event/event.component.css']
})
export class EventComponent implements OnInit {

    @Input('event') event: Event;

    constructor(private _eventService: EventService) {}

    ngOnInit() {
    }

}