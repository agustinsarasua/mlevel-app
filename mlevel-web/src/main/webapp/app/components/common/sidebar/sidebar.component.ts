import {Component, OnInit, Output, Input, EventEmitter} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from 'angular2/router';
import {MDL} from "./../MaterialDesignLiteUpgradeElement";

@Component({
    selector: 'm-sidebar',
    directives: [ROUTER_DIRECTIVES, MDL],
    providers: [ROUTER_PROVIDERS],
    templateUrl: 'app/components/common/sidebar/sidebar.component.html',
    styleUrls: ['app/components/common/sidebar/sidebar.component.css']
})
export class SidebarComponent implements OnInit {

    constructor() {
    }

    @Input('page-title') title: string;
    @Output() menuClick: EventEmitter = new EventEmitter();

    ngOnInit() {
    }

    openMenuEvent(evt){
        this.menuClick.emit([evt]);
    }

}