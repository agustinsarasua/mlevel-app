import {Component, OnInit, Input, Output, EventEmitter} from 'angular2/core';
import {Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS} from 'angular2/router';
import {MDL} from "./../MaterialDesignLiteUpgradeElement";
import {AuthenticationService} from "../../../services/authentication.service";
import {isLoggedin} from "../../../util/is-loggedin";

declare var FB: any;

@Component({
    selector: 'm-header',
    directives: [ROUTER_DIRECTIVES, MDL],
    providers: [ROUTER_PROVIDERS],
    templateUrl: 'app/components/common/header/header.component.html',
    styleUrls: ['app/components/common/header/header.component.css']
})
export class HeaderComponent implements OnInit {

    @Input('page-title') title: string;
    @Output() menuClick: EventEmitter = new EventEmitter();

    error: boolean = false;

    constructor(public auth: AuthenticationService, public router: Router){
    }

    ngOnInit() {
    }

    isUserLoggedIn(){
        return isLoggedin();
    }



    openMenuEvent(evt){
        this.menuClick.emit([evt]);
    }

    public loginWithServer(accessToken: string): void{
        this.auth.facebookLogin(accessToken)
            .subscribe(
                (token: any) => this.router.navigate(['EventList']),
                () => {this.error = true}
            );

    }

    login(){
        //FB.login();
        FB.login((response) => {
            // handle the response
            if (response.status === 'connected') {
                // Logged into your app and Facebook.
                console.log(response.authResponse.accessToken);
                localStorage.setItem("facebookUserId", response.authResponse.userID);
                this.loginWithServer(response.authResponse.accessToken);

            } else if (response.status === 'not_authorized') {
                // The person is logged into Facebook, but not your app.
            } else {
                // The person is not logged into Facebook, so we're not sure if
                // they are logged into this app or not.
            }
        }, {scope: 'public_profile,email,user_events'});
    }

    logout(){
        localStorage.removeItem('token');
        this.router.navigate(['Home']);
    }
}