import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router} from 'angular2/router';
import {SidebarComponent} from "./components/common/sidebar/sidebar.component";
import {HomeComponent} from "./components/home/home.component";
import {CreateEventComponent} from "./components/event/createEvent.component";
import {MDL} from "./components/common/MaterialDesignLiteUpgradeElement";
import {EventService} from "./services/event.service";
import {EventListComponent} from "./components/event/eventList.component";
import {HeaderComponent} from "./components/common/header/header.component";
import {AuthenticationService} from "./services/authentication.service";
import {FacebookService} from "./services/facebook.service";

@Component({
    selector: 'my-app',
    styleUrls: ['app/app.component.css'],
    templateUrl:'app/app.component.html',
    directives: [ROUTER_DIRECTIVES, MDL, HeaderComponent, SidebarComponent, EventListComponent],
    providers: [ROUTER_PROVIDERS, EventService, AuthenticationService, FacebookService]
})
@RouteConfig([
    {
        path: '/home',
        name: 'Home',
        component: HomeComponent,
        useAsDefault: true
    },
    {
        path: '/events',
        name: 'EventList',
        component: EventListComponent,
        useAsDefault: false
    },
    {
        path: '/events/add',
        name: 'CreateEvent',
        component: CreateEventComponent,
        useAsDefault: false
    }
])
export class AppComponent {
    title = 'Example';
    constructor() {
    }
}
