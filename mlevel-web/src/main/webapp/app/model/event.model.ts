import {Place} from "./place.model";

export class Event {
    public id: string;
    public name: string;
    public description: string;
    public imageUrl: string;
    public createdDate: Date;
    public dueDate: Date;

    constructor(public id:string, public name:string, public description:string) {}
}

export enum EventType{
    Concert,
    Movie,
    FootballMatch
}

export class FacebookEvent {
    public id: string;
    public description: string;
    public name: string;
    public place: Place;
    public startTime: Date;
    public endTime: Date;
    public cover: Cover;

    constructor(fbEvent: any){
        this.id = fbEvent.id;
        this.description = fbEvent.description;
        this.endTime = fbEvent.end_time;
        this.startTime = fbEvent.start_time;
        this.name = fbEvent.name;
        this.place = fbEvent.place != null ? new Place(fbEvent.place) : null;
    }
}

export class Cover {
    public id: string;
    public source: string;
    public offsetX: number;
    public offsetY: number;
}