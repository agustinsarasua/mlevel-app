import {Location} from './location.model'

export class Place {
    public id: string;
    public name: string;
    public location: Location;

    constructor(fbPlace: any){
        this.id = fbPlace.id;
        this.name = fbPlace.name;
        this.location = fbPlace.location != null ? new Location(fbPlace.location) : null;
    }
}