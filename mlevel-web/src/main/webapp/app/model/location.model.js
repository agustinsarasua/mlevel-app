System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Location;
    return {
        setters:[],
        execute: function() {
            Location = (function () {
                function Location(fbLocation) {
                    this.city = fbLocation.city ? fbLocation.city : null;
                    this.country = fbLocation.country != null ? fbLocation.country : null;
                    this.latitude = fbLocation.latitude ? fbLocation.latitude : null;
                    this.longitude = fbLocation.longitude ? fbLocation.longitude : null;
                    this.street = fbLocation.street ? fbLocation.street : null;
                    this.zip = fbLocation.zip ? fbLocation.zip : null;
                }
                return Location;
            }());
            exports_1("Location", Location);
        }
    }
});
//# sourceMappingURL=location.model.js.map