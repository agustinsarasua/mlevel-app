System.register(["./place.model"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var place_model_1;
    var Event, EventType, FacebookEvent, Cover;
    return {
        setters:[
            function (place_model_1_1) {
                place_model_1 = place_model_1_1;
            }],
        execute: function() {
            Event = (function () {
                function Event(id, name, description) {
                    this.id = id;
                    this.name = name;
                    this.description = description;
                }
                return Event;
            }());
            exports_1("Event", Event);
            (function (EventType) {
                EventType[EventType["Concert"] = 0] = "Concert";
                EventType[EventType["Movie"] = 1] = "Movie";
                EventType[EventType["FootballMatch"] = 2] = "FootballMatch";
            })(EventType || (EventType = {}));
            exports_1("EventType", EventType);
            FacebookEvent = (function () {
                function FacebookEvent(fbEvent) {
                    this.id = fbEvent.id;
                    this.description = fbEvent.description;
                    this.endTime = fbEvent.end_time;
                    this.startTime = fbEvent.start_time;
                    this.name = fbEvent.name;
                    this.place = fbEvent.place != null ? new place_model_1.Place(fbEvent.place) : null;
                }
                return FacebookEvent;
            }());
            exports_1("FacebookEvent", FacebookEvent);
            Cover = (function () {
                function Cover() {
                }
                return Cover;
            }());
            exports_1("Cover", Cover);
        }
    }
});
//# sourceMappingURL=event.model.js.map