export class Action {
    imageUrl: string;
    name: string;
    description: string;
}