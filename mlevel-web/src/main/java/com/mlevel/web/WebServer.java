package com.mlevel.web;

import com.mlevel.web.context.scan.WebContext;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.jetty.servlets.GzipFilter;
import org.eclipse.jetty.util.StringUtil;
import org.eclipse.jetty.util.resource.ResourceCollection;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.DispatcherType;
import java.util.EnumSet;
import java.util.TimeZone;

public class WebServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebServer.class);

    private static final String CONTEXT_NAME = "/mlevel";
    private static final String DISPLAY_NAME = "mlevel";
    private static final int DEFAULT_PORT = 9290;

    private final Server server;

    public WebServer(String[] args) throws Exception {
        this.server = this.createNewServer(args);
    }

    public static void main(String[] args) throws Exception {
        WebServer server = new WebServer(args);
        server.run();
    }

    private Server createNewServer(String[] args) throws Exception {
        int port = args.length > 0 ? Integer.parseInt(args[0]) : DEFAULT_PORT;

        Server server = new Server(port);
        WebAppContext handler = this.buildWebAppContext(args);
        server.setHandler(handler);
        server.setStopAtShutdown(true);

        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));

        return server;
    }

    private void run() throws Exception {
        this.server.start();
        LOGGER.info("Server Started");
        this.server.join();
    }

    private WebAppContext buildWebAppContext(String[] args) {

        AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        applicationContext.register(WebContext.class);
        LOGGER.info("Init WebAppContext");
        String contextPath = args.length > 1 ? args[1] : CONTEXT_NAME;
        WebAppContext handler = new WebAppContext();
        handler.setContextPath(contextPath);
        handler.setDisplayName(DISPLAY_NAME);
        handler.setWelcomeFiles(new String[] {"index.html"});
        handler.setInitParameter("useFileMappedBuffer", "false");

        String[] resources = new String[] {"./../src/main/webapp"};

        // Esta linea se usa para IntelliJ
        if (System.getProperty("IntelliJ") != null) {
            resources = new String[] {"./mlevel-web/src/main/webapp"};
        }

        handler.setBaseResource(new ResourceCollection(resources));
        handler.setResourceAlias("/WEB-INF/classes/", "/classes/");

        this.appendListeners(applicationContext, handler);
        this.appendErrorHandler(handler);
        this.appendSpringDispatcherServlet(applicationContext, handler);
        this.appendFilters(handler);

        applicationContext.close();
        return handler;
    }

    private void appendListeners(AnnotationConfigWebApplicationContext applicationContext, ServletContextHandler handler) {
        // Para que funcione Spring con su contexto
        handler.addEventListener(new ContextLoaderListener(applicationContext));
    }

    private void appendErrorHandler(WebAppContext handler) {
        // ErrorPageErrorHandler errorHandler = new ErrorPageErrorHandler();
        // errorHandler.addErrorPage(500, "/views/error/error.xhtml");
        // errorHandler.addErrorPage(404, "/views/error/urlError.xhtml");
        // handler.setErrorHandler(errorHandler);
    }

    private void appendSpringDispatcherServlet(AnnotationConfigWebApplicationContext applicationContext, ServletContextHandler handler) {
        LOGGER.info("Init Spring DispatcherServlet");

        DispatcherServlet dispatcherServlet = new DispatcherServlet(applicationContext);
        ServletHolder servletHolder = new ServletHolder(dispatcherServlet);
        servletHolder.setName("spring");
        servletHolder.setInitOrder(1);

        handler.addServlet(servletHolder, "/service/*");
    }

    private void appendFilters(ServletContextHandler handler) {

        DelegatingFilterProxy filter = new DelegatingFilterProxy("springSecurityFilterChain");
        FilterHolder springSecurityFilterHolder = new FilterHolder(filter);
        handler.addFilter(springSecurityFilterHolder, "/service/*", EnumSet.allOf(DispatcherType.class));

        LOGGER.info("Init Encoding Filter");
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding(StringUtil.__UTF8);
        FilterHolder characterEncodingFilterHolder = new FilterHolder(characterEncodingFilter);
        handler.addFilter(characterEncodingFilterHolder, "/*", EnumSet.of(DispatcherType.REQUEST, DispatcherType.ERROR));

        LOGGER.info("Init Gzip Filter");
        GzipFilter gzipFilter = new GzipFilter();
        FilterHolder gzipFilterHolder = new FilterHolder(gzipFilter);
        handler.addFilter(gzipFilterHolder, "/*", EnumSet.of(DispatcherType.REQUEST));

        // Cross Origin Filter
        LOGGER.info("Init Cross Origin Filter");
        FilterHolder crossOriginFilterHolder = new FilterHolder(CrossOriginFilter.class);
        crossOriginFilterHolder.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        crossOriginFilterHolder.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM,
                "origin, content-type, accept, authorization, x-requested-with, x-client, x-version");
        crossOriginFilterHolder.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,POST,DELETE,PUT,HEAD");
        handler.addFilter(crossOriginFilterHolder, "/*", EnumSet.of(DispatcherType.REQUEST, DispatcherType.ERROR));
    }

}
