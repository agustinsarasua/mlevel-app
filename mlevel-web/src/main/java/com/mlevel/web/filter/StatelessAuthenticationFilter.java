package com.mlevel.web.filter;

import com.mlevel.service.business.UserService;
import com.mlevel.service.context.config.ApplicationConfig;
import com.mlevel.service.model.authentication.UserAuthentication;
import com.mlevel.service.repository.dao.UserDAO;
import com.mlevel.service.business.authorization.AuthorizationRequestContext;
import com.mlevel.service.business.authorization.AuthorizationService;
import com.mlevel.service.business.authorization.impl.RequestSigningAuthorizationService;
import com.mlevel.service.business.authorization.impl.SessionTokenAuthorizationService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by agustin on 06/03/16.
 */
@Component
public class StatelessAuthenticationFilter extends GenericFilterBean {

    protected static final String HEADER_AUTHORIZATION = "Authorization";

    protected static final String HEADER_DATE = "x-mlevel-rest-date";

    protected static final String HEADER_NONCE = "nonce";

    private AuthorizationService authorizationService;

    //private RequestSigningAuthorizationService requestSigningAuthorizationService;

    private ApplicationConfig config;

    private String excludePatterns;

    @Autowired
    public StatelessAuthenticationFilter(ApplicationConfig config, AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
        this.config = config;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String url = this.endUrl(httpRequest);
        if(!this.matchExcludePatterns(url)) {
            String authToken = httpRequest.getHeader(HEADER_AUTHORIZATION);
            String requestDateString = httpRequest.getHeader(HEADER_DATE);
            String nonce = httpRequest.getHeader(HEADER_NONCE);
            AuthorizationRequestContext context = new AuthorizationRequestContext(httpRequest.getRequestURI(), httpRequest.getMethod(),
                    requestDateString, nonce, authToken);
            UserAuthentication externalUser = authorizationService.authorize(context);
            SecurityContextHolder.getContext().setAuthentication(externalUser);
        }
        chain.doFilter(request, response);
    }

    /**
     * Specify the AuthorizationService that the application should use
     */
//    private void delegateAuthorizationService(UserDAO userRepository, UserService userService, ApplicationConfig config) {
//        if(config.requireSignedRequests()) {
//            this.authorizationService = new RequestSigningAuthorizationService(userRepository, userService, config);
//        } else {
//            this.authorizationService = new SessionTokenAuthorizationService(userRepository);
//        }
//    }

    private String endUrl(HttpServletRequest httpServletRequest) {
        String fullUrl = httpServletRequest.getRequestURL().toString();
        String base = httpServletRequest.getContextPath();
        int idx = StringUtils.indexOf(fullUrl, base);
        return fullUrl.substring(idx + base.length());
    }

    private boolean matchExcludePatterns(String url) {
        boolean match = false;
        String[] patterns = this.excludePatterns.split(",");
        int i = 0;
        while(!match && i < patterns.length) {
            match = url.matches(patterns[i]);
            ++i;
        }
        return match;
    }


    public void setExcludePatterns(String excludePatterns) {
        this.excludePatterns = excludePatterns;
    }
}
