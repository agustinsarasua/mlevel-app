package com.mlevel.web.context.scan;

import com.mlevel.service.business.UserService;
import com.mlevel.service.business.authorization.impl.SessionTokenAuthorizationService;
import com.mlevel.service.context.config.ApplicationConfig;
import com.mlevel.service.model.authentication.Role;
import com.mlevel.web.filter.StatelessAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@Order(1)
public class SecurityContext extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationConfig config;

    @Autowired
    private SessionTokenAuthorizationService authorizationService;

    private final static String EXCLUDE_PATTERNS = ".*/service/user/login/.*$,.*/service/user/login$";

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        StatelessAuthenticationFilter statelessAuthenticationFilter = new StatelessAuthenticationFilter(config, authorizationService);
        statelessAuthenticationFilter.setExcludePatterns(EXCLUDE_PATTERNS);

        http.csrf().disable()
                .exceptionHandling().and()
                .anonymous().and()
                .authorizeRequests()

                        //allow anonymous resource requests
                .antMatchers("/").permitAll()
                .antMatchers("/favicon.ico").permitAll()
                .antMatchers("/resources/**").permitAll()

                //allow anonymous POSTs to login
                .antMatchers(HttpMethod.POST, "/service/password/tokens/*").permitAll()
                .antMatchers(HttpMethod.POST, "/service/password/tokens").permitAll()
                .antMatchers(HttpMethod.POST, "/service/tokens").permitAll()
                .antMatchers(HttpMethod.POST, "/service/tokens/*").permitAll()
                .antMatchers(HttpMethod.POST, "/service/user").permitAll()
                .antMatchers(HttpMethod.POST, "/service/user/login").permitAll()
                .antMatchers(HttpMethod.POST, "/service/user/login/*").permitAll()
                //all other request need to be authenticated
                .anyRequest().hasRole(Role.AUTHENTICATED.name()).and()

                // custom JSON based authentication by POST of {"username":"<name>","password":"<password>"} which sets the token header upon authentication
                //.addFilterBefore(new StatelessLoginFilter("/api/login", tokenAuthenticationService, userDetailsService, authenticationManager()), UsernamePasswordAuthenticationFilter.class)

                // custom Token based authentication based on the header previously given to the client
                .addFilterBefore(statelessAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected UserDetailsService userDetailsService() {
        return userService;
    }
}
