package com.mlevel.web.mapper;

import com.mlevel.api.ProfileDTO;
import com.mlevel.service.model.Profile;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProfileMapper extends AbstractMapper<Profile, ProfileDTO> {

    @Override
    public Profile mapToEntity(ProfileDTO dto) {
        Profile profile = new Profile();
        profile.setName(dto.getName());
        profile.setDiscovery(dto.isDiscovery());
        profile.setImgUrl(dto.getImgUrl());
        profile.setWelcomeMessage(dto.getWelcomeMessage());
        profile.setSex(dto.getSex());
        profile.setMaxAge(dto.getMaxAge());
        profile.setMinAge(dto.getMinAge());
        profile.setDiscoverySex(dto.getDiscoverySex());
        return profile;
    }

    @Override
    public List<Profile> mapToEntityList(List<ProfileDTO> dtos) {
        return null;
    }

    @Override
    public ProfileDTO mapToDTO(Profile entity) {
        ProfileDTO profile = new ProfileDTO();
        profile.setName(entity.getName());
        profile.setDiscovery(entity.isDiscovery());
        profile.setImgUrl(entity.getImgUrl());
        profile.setWelcomeMessage(entity.getWelcomeMessage());
        profile.setSex(entity.getSex());
        profile.setMaxAge(entity.getMaxAge());
        profile.setMinAge(entity.getMinAge());
        profile.setDiscoverySex(entity.getDiscoverySex());
        return profile;
    }

    @Override
    public List<ProfileDTO> mapToDTOList(List<Profile> entities) {
        return null;
    }
}
