package com.mlevel.web.mapper;

import com.mlevel.api.ActionDTO;
import com.mlevel.api.InvitationDTO;
import com.mlevel.service.datastore.model.InvitationEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InvitationMapper extends AbstractMapper<InvitationEntity, InvitationDTO> {

    @Override
    public InvitationEntity mapToEntity(InvitationDTO dto) {
        return null;
    }

    @Override
    public List<InvitationEntity> mapToEntityList(List<InvitationDTO> dtos) {
        return null;
    }

    @Override
    public InvitationDTO mapToDTO(InvitationEntity entity) {
        InvitationDTO invitationDTO = new InvitationDTO();
        invitationDTO.setActionDTO(new ActionDTO(entity.getActionName(), entity.getActionName()));
        invitationDTO.setInvitationDate(entity.getTimestamp().toDate());
        invitationDTO.setUuid(entity.getUuid());
        invitationDTO.setInvitationDueDate(entity.getDueDate().toDate());
        invitationDTO.setState(entity.getState().name());
        invitationDTO.setLastUpdate(entity.getLastUpdate().toDate());
        return invitationDTO;
    }

    @Override
    public List<InvitationDTO> mapToDTOList(List<InvitationEntity> entities) {
        return null;
    }
}
