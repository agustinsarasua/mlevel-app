package com.mlevel.web.mapper;

import com.mlevel.api.EventDTO;
import com.mlevel.api.location.PlaceDTO;
import com.mlevel.service.datastore.model.EventEntity;
import com.mlevel.service.datastore.model.PlaceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by macuco on 10/5/16.
 */
@Component
public class PlaceMapper extends AbstractMapper<PlaceEntity, PlaceDTO> {

    @Autowired
    private LocationMapper locationMapper;

    @Override
    public PlaceEntity mapToEntity(PlaceDTO dto) {
        return null;
    }

    @Override
    public List<PlaceEntity> mapToEntityList(List<PlaceDTO> placeDTOs) {
        return null;
    }

    @Override
    public PlaceDTO mapToDTO(PlaceEntity entity) {
        PlaceDTO dto = new PlaceDTO();
        dto.setUuid(entity.getUuid());
        dto.setFacebookId(entity.getFacebookId());
        dto.setName(entity.getName());
        dto.setLocation(locationMapper.mapToDTO(entity.getLocation()));
        return dto;
    }

    @Override
    public List<PlaceDTO> mapToDTOList(List<PlaceEntity> entities) {
        return null;
    }
}
