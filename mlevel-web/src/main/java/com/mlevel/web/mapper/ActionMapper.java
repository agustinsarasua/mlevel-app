package com.mlevel.web.mapper;

import com.mlevel.api.ActionDTO;
import com.mlevel.service.datastore.model.ActionEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ActionMapper extends AbstractMapper<ActionEntity, ActionDTO> {

    @Override
    public ActionEntity mapToEntity(ActionDTO dto) {
        return null;
    }

    @Override
    public List<ActionEntity> mapToEntityList(List<ActionDTO> dtos) {
        return null;
    }

    @Override
    public ActionDTO mapToDTO(ActionEntity entity) {
        ActionDTO dto = new ActionDTO();
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        return dto;
    }

    @Override
    public List<ActionDTO> mapToDTOList(List<ActionEntity> entities) {
        return entities.stream().map(e -> this.mapToDTO(e)).collect(Collectors.toList());
    }
}
