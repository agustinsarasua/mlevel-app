package com.mlevel.web.mapper;

import com.mlevel.api.location.LocationDTO;
import com.mlevel.api.timeline.TimelineItemDTO;
import com.mlevel.service.datastore.model.EventEntity;
import com.mlevel.service.elastic.SearchEventEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by agustin on 02/04/16.
 */
@Component
public class TimelineEventMapper extends AbstractMapper<EventEntity, TimelineItemDTO> {

    @Autowired
    private ActionMapper actionMapper;

    @Override
    public EventEntity mapToEntity(TimelineItemDTO dto) {

        return null;
    }

    @Override
    public List<EventEntity> mapToEntityList(List<TimelineItemDTO> dtos) {
        return null;
    }

    @Override
    public TimelineItemDTO mapToDTO(EventEntity entity) {
        TimelineItemDTO item = new TimelineItemDTO();
        //item.setImageUrl(entity.getImageUrl());
        item.setDescription(entity.getDescription());
        //item.setDueDate(entity.getDueDate().toDate());
        //item.setItemType(entity.getEventType());
        //item.setCreationDate(entity.getCreatedDate().toDate());
        //item.setCommonActions(this.actionMapper.mapToDTOList(entity.getCommonActions()));
        //item.setItemUuid(entity.getUuid());
        //item.setLocationDTO(new LocationDTO(entity.getLocation().getLat(), entity.getLocation().getLon()));
        return item;
    }

    @Override
    public List<TimelineItemDTO> mapToDTOList(List<EventEntity> entities) {
        return null;
    }
}
