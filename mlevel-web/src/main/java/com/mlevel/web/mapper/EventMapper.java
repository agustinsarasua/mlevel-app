package com.mlevel.web.mapper;

import com.mlevel.api.EventDTO;
import com.mlevel.api.location.LocationDTO;
import com.mlevel.api.timeline.TimelineItemDTO;
import com.mlevel.service.datastore.model.EventEntity;
import com.mlevel.service.elastic.SearchEventEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EventMapper extends AbstractMapper<EventEntity, EventDTO> {

    @Autowired
    private ActionMapper actionMapper;

    @Autowired
    private PlaceMapper placeMapper;

    @Override
    public EventEntity mapToEntity(EventDTO dto) {
        return null;
    }

    @Override
    public List<EventEntity> mapToEntityList(List<EventDTO> dtos) {
        return null;
    }

    @Override
    public EventDTO mapToDTO(EventEntity entity) {
        EventDTO eventDTO = new EventDTO();
        eventDTO.setUuid(entity.getUuid());
        eventDTO.setName(entity.getName());
        eventDTO.setDescription(entity.getDescription());
        eventDTO.setImageUrl(entity.getImageUrl());
        eventDTO.setType(entity.getType());
        //eventDTO.setTimelineItemType(TimelineItemDTO.TimelineItemType.valueOf(entity.getTimelineItemType()));
        eventDTO.setCreationDate(entity.getCreationDate().toDate());
        eventDTO.setStartTime(entity.getStartTime().toDate());
        eventDTO.setEndTime(entity.getEndTime().toDate());
        eventDTO.setSubtitle(entity.getSubtitle());
        eventDTO.setCommonActions(this.actionMapper.mapToDTOList(entity.getCommonActions()));
        eventDTO.setPlace(this.placeMapper.mapToDTO(entity.getPlace()));
        return eventDTO;
    }

    @Override
    public List<EventDTO> mapToDTOList(List<EventEntity> entities) {
        return null;
    }
}
