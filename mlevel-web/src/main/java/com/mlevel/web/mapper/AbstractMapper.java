package com.mlevel.web.mapper;

import com.mlevel.api.AbstractDTO;

import java.io.Serializable;
import java.util.List;

/**
 * Created by agustin on 29/02/16.
 */
public abstract class AbstractMapper<T extends Serializable, DTO extends AbstractDTO> {

    public abstract T mapToEntity(DTO dto);

    public abstract List<T> mapToEntityList(List<DTO> dtos);

    public abstract DTO mapToDTO(T entity);

    public abstract List<DTO> mapToDTOList(List<T> entities);
}
