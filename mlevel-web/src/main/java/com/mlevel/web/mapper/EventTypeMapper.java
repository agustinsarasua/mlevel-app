package com.mlevel.web.mapper;

import com.mlevel.api.EventTypeDTO;
import com.mlevel.service.model.EventType;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EventTypeMapper extends AbstractMapper<EventType, EventTypeDTO> {

    @Override
    public EventType mapToEntity(EventTypeDTO dto) {
        EventType eventType = new EventType();
        eventType.setName(dto.getName());
        eventType.setDescription(dto.getDescription());
        return eventType;
    }

    @Override
    public List<EventType> mapToEntityList(List<EventTypeDTO> dtos) {
        return null;
    }

    @Override
    public EventTypeDTO mapToDTO(EventType entity) {
        EventTypeDTO eventTypeDTO = new EventTypeDTO();
        eventTypeDTO.setName(entity.getName());
        eventTypeDTO.setDescription(entity.getDescription());
        return eventTypeDTO;
    }

    @Override
    public List<EventTypeDTO> mapToDTOList(List<EventType> entities) {
        return null;
    }
}
