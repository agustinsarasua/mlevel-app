package com.mlevel.web.mapper;

import com.mlevel.api.location.LocationDTO;
import com.mlevel.service.datastore.model.LocationEntity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by macuco on 10/5/16.
 */
@Component
public class LocationMapper extends AbstractMapper<LocationEntity, LocationDTO> {


    @Override
    public LocationEntity mapToEntity(LocationDTO dto) {
        return null;
    }

    @Override
    public List<LocationEntity> mapToEntityList(List<LocationDTO> locationDTOs) {
        return null;
    }

    @Override
    public LocationDTO mapToDTO(LocationEntity entity) {
        LocationDTO dto = new LocationDTO();
        dto.setCountry(entity.getCountry());
        dto.setCity(entity.getCity());
        dto.setAddress(entity.getAddress());
        dto.setLatitude(entity.getLatitude());
        dto.setLongitude(entity.getLongitude());
        return dto;
    }

    @Override
    public List<LocationDTO> mapToDTOList(List<LocationEntity> entities) {
        return null;
    }
}
