package com.mlevel.web.mapper;

import com.mlevel.api.authentication.UserDTO;
import com.mlevel.service.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserMapper extends AbstractMapper<User, UserDTO> {

    @Override
    public User mapToEntity(UserDTO dto) {
        User user = new User();
        user.setBirthday(dto.getBirthday());
        user.setEmail(dto.getEmail());
        user.setName(dto.getName());
        user.setLastName(dto.getLastName());
        return user;
    }

    @Override
    public List<User> mapToEntityList(List<UserDTO> dtos) {
        return null;
    }

    @Override
    public UserDTO mapToDTO(User entity) {
        return null;
    }

    @Override
    public List<UserDTO> mapToDTOList(List<User> entities) {
        return null;
    }
}
