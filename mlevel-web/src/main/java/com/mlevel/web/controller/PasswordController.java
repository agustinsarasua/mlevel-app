package com.mlevel.web.controller;

import com.mlevel.api.authentication.request.LostPasswordRequestDTO;
import com.mlevel.api.authentication.request.PasswordRequestDTO;
import com.mlevel.service.business.authentication.VerificationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;

@RestController
@RequestMapping(value = "/password")
public class PasswordController {

    @Autowired
    protected VerificationTokenService verificationTokenService;

    @PermitAll
    @RequestMapping(value="/tokens", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void sendEmailToken(LostPasswordRequestDTO request) {
        verificationTokenService.sendLostPasswordToken(request);
    }

    @RequestMapping(value="/tokens/{token}", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public void resetPassword(@PathVariable(value = "token") String base64EncodedToken, @RequestBody PasswordRequestDTO request) {
        verificationTokenService.resetPassword(base64EncodedToken, request);
    }
}
