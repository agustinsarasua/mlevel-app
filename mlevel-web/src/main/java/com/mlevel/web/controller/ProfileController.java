package com.mlevel.web.controller;

import com.mlevel.api.ProfileDTO;
import com.mlevel.service.business.ProfileService;
import com.mlevel.service.model.Profile;
import com.mlevel.service.model.User;
import com.mlevel.service.model.authentication.UserAuthentication;
import com.mlevel.web.mapper.ProfileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/profile")
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @Autowired
    private ProfileMapper profileMapper;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void createProfile(@RequestBody ProfileDTO profileDTO) {
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        //Profile profile = profileMapper.mapToEntity(profileDTO);
        //profile.setUser(user);
        profileService.createProfile(user, profileDTO);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.OK)
    public void updateProfile(@RequestBody ProfileDTO profileDTO) {
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        //Profile profile = profileMapper.mapToEntity(profileDTO);
        //profile.setUser(user);
        profileService.updateProfile(user, profileDTO);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public HttpEntity<ProfileDTO> getProfile() {
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        ProfileDTO profile = profileMapper.mapToDTO(user.getProfile());
        return new ResponseEntity<>(profile, HttpStatus.OK);
    }

}
