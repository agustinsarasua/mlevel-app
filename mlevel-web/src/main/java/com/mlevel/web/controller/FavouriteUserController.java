package com.mlevel.web.controller;

import com.mlevel.service.business.FavouriteUserService;
import com.mlevel.service.datastore.model.FavouriteUserEntity;
import com.mlevel.service.model.User;
import com.mlevel.service.model.authentication.UserAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/favourite-user")
public class FavouriteUserController {

    @Autowired
    private FavouriteUserService favouriteUserService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void addFavouriteUser(@PathVariable String userId) {
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        favouriteUserService.addFavouriteUser(user, userId);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeFavouriteUser(@PathVariable String userId) {
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        favouriteUserService.removeFavouriteUser(user, userId);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public HttpEntity loadFavouriteUsers() {
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        List<FavouriteUserEntity> favouriteUserEntityList = favouriteUserService.loadFavouriteUsers(user);
        return new ResponseEntity(null, HttpStatus.OK);
    }
}
