package com.mlevel.web.controller;

import com.mlevel.api.location.LocationDTO;
import com.mlevel.api.timeline.TimelineItemDTO;
import com.mlevel.service.business.InvitationService;
import com.mlevel.service.business.LocationService;
import com.mlevel.service.business.SearchService;
import com.mlevel.service.datastore.model.EventEntity;
import com.mlevel.service.elastic.SearchEventEntity;
import com.mlevel.service.elastic.UserEntity;
import com.mlevel.service.model.User;
import com.mlevel.service.model.authentication.UserAuthentication;
import com.mlevel.web.mapper.TimelineEventMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = "/timeline")
public class TimelineController {

    @Autowired
    private SearchService searchService;

    @Autowired
    private InvitationService invitationService;

    @Autowired
    private TimelineEventMapper timelineEventMapper;

    @Autowired
    private LocationService locationService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public HttpEntity<List<TimelineItemDTO>> loadTimelineItems() {
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        UserEntity userEntity = locationService.loadUserEntity(user.getUuid().toString());
        /*List<EventEntity> nearbyEvents = searchService.loadEventsNear(new LocationDTO(userEntity.getLocation().getLat(), userEntity.getLocation().getLon()), 1000);

        List<TimelineItemDTO> result = new ArrayList<>();
        nearbyEvents.stream().forEach(eventEntity -> {
            TimelineItemDTO itemDTO = this.timelineEventMapper.mapToDTO(eventEntity);
            result.add(itemDTO);
        });
        */
        //return new ResponseEntity<>(result, HttpStatus.OK);
        return new ResponseEntity<>(Arrays.asList(), HttpStatus.OK);
    }
}
