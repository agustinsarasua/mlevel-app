package com.mlevel.web.controller;

import com.mlevel.api.InvitationDTO;
import com.mlevel.service.business.EventService;
import com.mlevel.service.business.InvitationService;
import com.mlevel.service.datastore.model.EventEntity;
import com.mlevel.service.datastore.model.InvitationEntity;
import com.mlevel.service.elastic.SearchEventEntity;
import com.mlevel.service.model.User;
import com.mlevel.service.model.authentication.UserAuthentication;
import com.mlevel.web.mapper.EventMapper;
import com.mlevel.web.mapper.InvitationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/invitation")
public class InvitationController {

    @Autowired
    private InvitationService invitationService;

    @Autowired
    private InvitationMapper invitationMapper;

    @Autowired
    private EventService eventService;

    @Autowired
    private EventMapper eventMapper;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void createInvitation(@RequestBody InvitationDTO invitationDTO){
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        invitationService.createInvitation(user, invitationDTO);
    }

    /**
     * GET Invitations made by me
     */
    @RequestMapping(value = "/invites", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public HttpEntity<List<InvitationDTO>> getInvitationsMadeByMe(){
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        List<InvitationEntity> invitationEntities = invitationService.loadInvitationsMadeByMe(user);
        List<InvitationDTO> result = new ArrayList<>();
        invitationEntities.stream().forEach(invitationEntity -> {
            InvitationDTO invitationDTO = this.invitationMapper.mapToDTO(invitationEntity);
            EventEntity eventEntity = eventService.loadEvent(invitationEntity.getEventId());
            invitationDTO.setEventDTO(eventMapper.mapToDTO(eventEntity));
            result.add(invitationDTO);
        });
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * GET Invitations made to me
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public HttpEntity<List<InvitationDTO>> getInvitationsMadeToMe(){
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        List<InvitationEntity> invitationEntities = invitationService.loadInvitationsMadeToMe(user);
        List<InvitationDTO> result = new ArrayList<>();
        invitationEntities.stream().forEach(invitationEntity -> {
            InvitationDTO invitationDTO = this.invitationMapper.mapToDTO(invitationEntity);
            EventEntity eventEntity = eventService.loadEvent(invitationEntity.getEventId());
            invitationDTO.setEventDTO(eventMapper.mapToDTO(eventEntity));
            result.add(invitationDTO);
        });
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * PATCH Accept Invitation
     */
    @RequestMapping(value="/{invitationId}/accept", method = RequestMethod.PATCH)
    @ResponseStatus(value = HttpStatus.OK)
    public void acceptInvitation(@PathVariable("invitationId") String invitationId){
        //UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        //User user = (User)externalUser.getDetails();
        invitationService.acceptInvitation(invitationId);
    }

    /**
     * PATCH Reject Invitation
     */
    @RequestMapping(value="/{invitationId}/reject", method = RequestMethod.PATCH)
    @ResponseStatus(value = HttpStatus.OK)
    public void rejectInvitation(@PathVariable("invitationId") String invitationId){
        //UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        //User user = (User)externalUser.getDetails();
        invitationService.rejectInvitation(invitationId);
    }
}
