package com.mlevel.web.controller;

import com.mlevel.api.location.LocationDTO;
import com.mlevel.service.business.LocationService;
import com.mlevel.service.model.User;
import com.mlevel.service.model.authentication.UserAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/location")
public class LocationController {

    @Autowired
    private LocationService locationService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void updateLocation(@RequestBody LocationDTO locationDTO) {
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        locationService.updateUserLocation(user, locationDTO.getLatitude(), locationDTO.getLongitude());
    }

}
