package com.mlevel.web.controller;

import com.mlevel.api.authentication.AuthenticatedUserTokenDTO;
import com.mlevel.api.authentication.ExternalUserDTO;
import com.mlevel.api.authentication.request.CreateUserRequestDTO;
import com.mlevel.api.authentication.request.LoginRequestDTO;
import com.mlevel.api.authentication.request.OAuth2RequestDTO;
import com.mlevel.service.business.UserService;
import com.mlevel.service.business.authentication.VerificationTokenService;
import com.mlevel.service.business.exception.BusinessException;
import com.mlevel.service.model.authentication.Role;
import com.mlevel.web.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private VerificationTokenService verificationTokenService;

    @Autowired
    private ConnectionFactoryLocator connectionFactoryLocator;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<?> add(@RequestBody CreateUserRequestDTO requestDTO) {
        try {
            AuthenticatedUserTokenDTO token = userService.createUser(requestDTO, Role.AUTHENTICATED);
            verificationTokenService.sendEmailRegistrationToken(token.getUserId());
            return new ResponseEntity<>(token, HttpStatus.CREATED);
        } catch (BusinessException e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/login/{providerId}", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public HttpEntity<AuthenticatedUserTokenDTO> socialLogin(@PathVariable(value = "providerId") String providerId, @RequestBody OAuth2RequestDTO request) {
        OAuth2ConnectionFactory<?> connectionFactory = (OAuth2ConnectionFactory<?>) connectionFactoryLocator.getConnectionFactory(providerId);
        Connection<?> connection = connectionFactory.createConnection(new AccessGrant(request.getAccessToken()));
        AuthenticatedUserTokenDTO token = userService.socialLogin(connection);
        return new ResponseEntity<>(token, HttpStatus.OK);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public HttpEntity<AuthenticatedUserTokenDTO> login(@RequestBody LoginRequestDTO request) {
        AuthenticatedUserTokenDTO token = userService.login(request);
        return new ResponseEntity<>(token, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public String getUserLogged() {
        String userEmailLogged = SecurityContextHolder.getContext().getAuthentication().getName();
        return userEmailLogged;
    }


}
