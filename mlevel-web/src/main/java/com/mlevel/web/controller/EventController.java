package com.mlevel.web.controller;

import com.google.common.collect.ImmutableMap;
import com.mlevel.api.EventDTO;
import com.mlevel.api.EventTypeDTO;
import com.mlevel.service.business.EventService;
import com.mlevel.service.datastore.model.EventEntity;
import com.mlevel.service.model.EventType;
import com.mlevel.service.model.User;
import com.mlevel.service.model.authentication.UserAuthentication;
import com.mlevel.web.mapper.EventMapper;
import com.mlevel.web.mapper.EventTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/event")
public class EventController {

    @Autowired
    private EventService eventService;

    @Autowired
    private EventMapper eventMapper;

    @Autowired
    private EventTypeMapper eventTypeMapper;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public HttpEntity<Map<String,String>> createEvent(@RequestBody EventDTO eventDTO) {
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        String eventId = eventService.createEvent(eventDTO);
        return new ResponseEntity<>(ImmutableMap.of("uuid",eventId),HttpStatus.OK);
    }

    @RequestMapping(value = "/{event_id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public HttpEntity<EventDTO> getEvent(@PathVariable(value = "event_id") String eventId) {
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        EventEntity event = eventService.loadEvent(eventId);
        return new ResponseEntity<>(eventMapper.mapToDTO(event), HttpStatus.OK);
    }

    @RequestMapping(value = "/types", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public HttpEntity<List<EventTypeDTO>> getEventTypes() {
        UserAuthentication externalUser = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        User user = (User)externalUser.getDetails();
        List<EventType> eventTypes = eventService.loadEventTypes();
        List<EventTypeDTO> result = eventTypes.stream().map(eventType -> eventTypeMapper.mapToDTO(eventType)).collect(Collectors.toList());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/types", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void createEventType(@RequestBody EventTypeDTO eventTypeDTO) {
        eventService.createEventType(eventTypeDTO);
    }
}
