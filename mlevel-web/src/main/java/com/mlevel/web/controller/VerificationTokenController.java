package com.mlevel.web.controller;

import com.mlevel.api.authentication.request.EmailVerificationRequestDTO;
import com.mlevel.service.business.authentication.VerificationTokenService;
import com.mlevel.service.model.authentication.VerificationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/tokens")
public class VerificationTokenController {

    @Autowired
    protected VerificationTokenService verificationTokenService;

    @RequestMapping(value="/{token}", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public HttpEntity<VerificationToken> verifyToken(@PathVariable(value = "token") String token) {
        VerificationToken result = verificationTokenService.verify(token);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void sendEmailToken(@RequestBody EmailVerificationRequestDTO request) {
        verificationTokenService.generateEmailVerificationToken(request.getEmailAddress());
    }

}
