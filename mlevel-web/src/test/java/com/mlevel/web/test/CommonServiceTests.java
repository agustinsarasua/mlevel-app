package com.mlevel.web.test;

import com.mlevel.api.ActionDTO;
import com.mlevel.api.InvitationDTO;
import com.mlevel.api.authentication.UserDTO;
import com.mlevel.api.location.LocationDTO;
import com.mlevel.service.business.FavouriteUserService;
import com.mlevel.service.business.InvitationService;
import com.mlevel.service.business.SearchService;
import com.mlevel.service.business.UserService;
import com.mlevel.service.datastore.model.FavouriteUserEntity;
import com.mlevel.service.datastore.model.InvitationEntity;
import com.mlevel.service.elastic.UserEntity;
import com.mlevel.service.model.User;
import com.mlevel.web.context.scan.WebContext;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class CommonServiceTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonServiceTests.class);

    @Autowired
    private SearchService searchService;

    @Autowired
    private InvitationService invitationService;

    @Autowired
    private FavouriteUserService favouriteUserService;

    @Autowired
    private UserService userService;

    private User user;

    @BeforeClass
    public static void setUpOnce(){}

    @Before
    public void setUp() throws Exception {
        user = (User) userService.loadUserByUsername("agustinsarasua@gmail.com");
    }

    @Test
    public void testFavouriteUsersCreation(){
        LOGGER.info("Testing Favourite Users Creation...");
        List<FavouriteUserEntity> favUsers = this.favouriteUserService.loadFavouriteUsers(user);
        Assert.notEmpty(favUsers);
        Assert.isTrue(favUsers.size() == 7);
        LOGGER.info("Testing Favourite Users Creation... DONE");
    }

    @Test
    public void testNearbyUsers(){
        LOGGER.info("Testing Loading Nearby Users...");
        LocationDTO dto = new LocationDTO(-34.921373, -56.150737);
        List<UserEntity> nearby = this.searchService.loadNearbyUsers(user, dto);
        Assert.notEmpty(nearby);
        LOGGER.info("Testing Loading Nearby Users... DONE");
    }

    @Test
    public void testCreateInvitation(){

        LOGGER.info("Testing Create Invitation...");
        LocationDTO dto = new LocationDTO(-34.921373, -56.150737);
        List<UserEntity> nearby = this.searchService.loadNearbyUsers(user, dto);
        Assert.notEmpty(nearby);

        UserEntity nearUser = nearby.get(0);
        UserDTO userDTO = new UserDTO();
        userDTO.setUuid(nearUser.getId());

        InvitationDTO invitationDTO = new InvitationDTO();
        invitationDTO.setActionDTO(new ActionDTO("Invitation Test", "http://img"));
        invitationDTO.setInvitationDueDate(new Date());
        invitationDTO.setUserDTO(userDTO);
        invitationService.createInvitation(user, invitationDTO);

        List<InvitationEntity> invitationEntities = invitationService.loadInvitationsMadeByMe(user);
        Assert.isTrue(invitationEntities.size() == 1);
        LOGGER.info("Testing Create Invitation... DONE");
    }

}
