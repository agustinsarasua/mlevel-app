package com.mlevel.web.test;

import com.mlevel.service.business.LocationService;
import com.mlevel.service.elastic.UserEntity;
import com.mlevel.service.elastic.builder.UserEntityBuilder;
import com.mlevel.service.elastic.repository.UserEntityRepository;
import com.mlevel.web.context.scan.WebContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class ElasticsearchTemplateTests {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private LocationService locationService;

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Before
    public void emptyData() throws Exception {
        userEntityRepository.deleteAll();
        /**
         * -34.921373, -56.150737   Agustin
         * -34.920651, -56.153612   Club Bigua
         * -34.920511, -56.149342   21 y Rmbla
         * -34.914898, -56.148784   21 y Av Brasil
         * -34.909742, -56.166787   21 y BR España
         * -34.905959, -56.163483   Av Brasil y Br Artigas
         * -34.908898, -56.154578   Av Brasil y Libertad
         * -34.897494, -56.124601   Rivera y Batlle Ordeñez
         * -34.881812, -56.093316   Parque Rivera
         * -34.894080, -56.153162   Estadio Centenario
         * -34.891106, -56.186442   Palacio Legislativo
         * -34.869806, -56.167603   Cuernos
         * -34.902616, -56.124516   Cementerio Buceo
         * -34.881847, -56.079969   Portones Shopping
         */
        elasticsearchTemplate.createIndex(UserEntity.class);
        elasticsearchTemplate.putMapping(UserEntity.class);
        List<IndexQuery> indexQueries = new ArrayList<>();
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.921373, -56.150737).sex("M").name("Agustin").buildIndex());
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.920651, -56.153612).sex("F").name("Club Bigua").buildIndex());
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.920511, -56.149342).sex("F").name("21 y Rmbla").buildIndex());
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.914898, -56.148784).sex("F").name("21 y Av Brasil").buildIndex());
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.909742, -56.166787).sex("F").name("21 y BR España").buildIndex());
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.905959, -56.163483).sex("F").name("Av Brasil y Br Artigas").buildIndex());
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.908898, -56.154578).sex("F").name("Av Brasil y Libertad").buildIndex());
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.897494, -56.124601).sex("F").name("Rivera y Batlle Ordeñez").buildIndex());
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.881812, -56.093316).sex("F").name("Parque Rivera").buildIndex());
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.894080, -56.153162).sex("M").name("Estadio Centenario").buildIndex());
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.891106, -56.186442).sex("M").name("Palacio Legislativo").buildIndex());
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.869806, -56.167603).sex("M").name("Cuernos").buildIndex());
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.902616, -56.124516).sex("M").name("Cementerio Buceo").buildIndex());
        indexQueries.add(new UserEntityBuilder(this.generateUUID()).location(-34.881847, -56.079969).sex("M").name("Portones Shopping").buildIndex());
        elasticsearchTemplate.bulkIndex(indexQueries);
    }

    @Test
    public void testSample(){
        CriteriaQuery geoLocationCriteriaQuery2 = new CriteriaQuery(
                new Criteria("sex").is("F").and("location").within(new GeoPoint(-34.921373, -56.150737), "1km"));
        List<UserEntity> geoAuthorsForGeoCriteria2 = elasticsearchTemplate.queryForList(geoLocationCriteriaQuery2, UserEntity.class);
        assertNotNull(geoAuthorsForGeoCriteria2);

    }

    private String generateUUID(){
        return UUID.randomUUID().toString();
    }
}
