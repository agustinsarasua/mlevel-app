package com.mlevel.web.test;

import com.mlevel.api.ActionDTO;
import com.mlevel.api.EventDTO;
import com.mlevel.api.location.LocationDTO;
import com.mlevel.api.location.PlaceDTO;
import com.mlevel.api.timeline.TimelineItemDTO;
import com.mlevel.service.business.EventService;
import com.mlevel.service.business.UserService;
import com.mlevel.service.datastore.model.EventEntity;
import com.mlevel.service.elastic.SearchEventEntity;
import com.mlevel.service.model.EventType;
import com.mlevel.service.model.User;
import com.mlevel.web.context.scan.WebContext;
import org.joda.time.MutableDateTime;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class EventServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private EventService eventService;

    private User user;

    @BeforeClass
    public static void setUpOnce(){}

    @Before
    public void setUp() throws Exception {
        user = (User) userService.loadUserByUsername("agustinsarasua@gmail.com");
    }

    @Test
    public void testLoadEventTypes(){
        List<EventType> eventTypes = eventService.loadEventTypes();
        Assert.isTrue(eventTypes.size() == 2);
    }

    @Test
    public void testCreateEvent(){
        MutableDateTime dateTime = new MutableDateTime(new Date());

        EventDTO eventDTO = new EventDTO();
        eventDTO.setType("OPEN_INVITATION");
        eventDTO.setName("Clasico Peñarol - Nacional");
        eventDTO.setSubtitle("....");
        eventDTO.setDescription("Se juega el clasico mas importante del mundo");
        eventDTO.setImageUrl("http://loremflickr.com/320/120/soccer");

        dateTime.addDays(2);
        eventDTO.setStartTime(dateTime.toDate());
        dateTime.addHours(5);
        eventDTO.setEndTime(dateTime.toDate());

        eventDTO.setTimelineItemType(TimelineItemDTO.TimelineItemType.EVENT);
        eventDTO.setPlace(this.createPlace());
        eventDTO.setCommonActions(this.createCommonActions());

        String eventId = eventService.createEvent(eventDTO);
        EventEntity eventEntity = eventService.loadEvent(eventId);

        Assert.notNull(eventEntity);
    }

    private PlaceDTO createPlace() {
        PlaceDTO dto = new PlaceDTO();
        dto.setUuid("d2fa26ed-2f46-4033-918a-3be6436525fd");
        dto.setName("Bar lo del Joeputa");
        dto.setFacebookId("a42a26ed-2e13-3063-97b-4be643652314");
        dto.setLocation(this.createLocation());
        return dto;
    }

    private LocationDTO createLocation() {
        LocationDTO dto = new LocationDTO();
        dto.setCountry("Uruguay");
        dto.setCity("Montevideo");
        dto.setAddress("En algun lugar de Pocitos");
        dto.setLatitude(-34.894080);
        dto.setLongitude(-56.153162);
        return dto;
    }

    private List<ActionDTO> createCommonActions() {
        List<ActionDTO> dtos = new ArrayList<>();

        ActionDTO dto = new ActionDTO();
        dto.setName("Participar");
        dto.setDescription("Descripcion de action 1");
        dtos.add(dto);
        dto = new ActionDTO();
        dto.setName("Ni idea");
        dto.setDescription("Descripcion de action 2");
        dtos.add(dto);

        return dtos;
    }

}
