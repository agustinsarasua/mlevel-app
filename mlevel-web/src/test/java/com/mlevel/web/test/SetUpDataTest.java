package com.mlevel.web.test;

import com.mlevel.web.context.scan.WebContext;
import com.mlevel.web.test.service.TestConfigurationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebContext.class)
@WebAppConfiguration
public class SetUpDataTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SetUpDataTest.class);

    @Autowired
    private TestConfigurationService testConfigurationService;

    @Before
    public void setUp(){
        testConfigurationService.setUp();
    }

    @Test
    public void setUpFinished(){
        LOGGER.info("Set up finished");
    }

}
