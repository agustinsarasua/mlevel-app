package com.mlevel.web.test.service;

import com.mlevel.api.EventTypeDTO;
import com.mlevel.api.ProfileDTO;
import com.mlevel.api.authentication.AuthenticatedUserTokenDTO;
import com.mlevel.api.authentication.ExternalUserDTO;
import com.mlevel.api.authentication.request.CreateUserRequestDTO;
import com.mlevel.api.authentication.request.PasswordRequestDTO;
import com.mlevel.service.business.*;
import com.mlevel.service.elastic.UserEntity;
import com.mlevel.service.elastic.builder.UserEntityBuilder;
import com.mlevel.service.elastic.repository.EventEntityRepository;
import com.mlevel.service.elastic.repository.UserEntityRepository;
import com.mlevel.service.model.User;
import com.mlevel.service.model.authentication.Role;
import com.mlevel.service.repository.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class TestConfigurationService {

    @Autowired
    private FavouriteUserService favouriteUserService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserService userService;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private EventService eventService;

    @Autowired
    private EventEntityRepository eventEntityRepository;

    private List<UserEntity> userEntities;

    {
        userEntities = new ArrayList<>();
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.921373, -56.150737).age(18).sex("M").name("Agustin").build());
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.920651, -56.153612).age(20).sex("F").name("0.27km").build());
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.920511, -56.149342).age(23).sex("F").name("0.16km").build());
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.914898, -56.148784).age(34).sex("M").name("0.74km").build());
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.909742, -56.166787).age(15).sex("F").name("1.95km").build());
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.905959, -56.163483).age(45).sex("F").name("2.07km").build());
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.908898, -56.154578).age(56).sex("F").name("1.43km").build());
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.897494, -56.124601).age(19).sex("F").name("3.57km").build());
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.881812, -56.093316).age(20).sex("F").name("6.84km").build());
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.894080, -56.153162).age(26).sex("M").name("3.04km").build());
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.891106, -56.186442).age(21).sex("M").name("4.68km").build());
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.869806, -56.167603).age(22).sex("F").name("5.92km").build());
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.902616, -56.124516).age(20).sex("M").name("3.17km").build());
        userEntities.add(new UserEntityBuilder(this.generateUUID()).location(-34.881847, -56.079969).age(78).sex("M").name("7.81km").build());
    }

    @Transactional
    public User setUp(){
        this.resetElasticSearch();
        this.createEventTypes();
        User user = this.createRootUser();
        this.createRandomUsers();
        this.createRootUserFavouriteUsers(user);
        return user;
    }

    public void resetElasticSearch(){
        userEntityRepository.deleteAll();
        eventEntityRepository.deleteAll();
    }

     /*
     * -34.921373, -56.150737   Agustin - (Referencia)
     * -34.920651, -56.153612   Club Bigua - (0.27km)
     * -34.920511, -56.149342   21 y Rmbla - (0.16km)
     * -34.914898, -56.148784   21 y Av Brasil - (0.74km)
     * -34.909742, -56.166787   21 y BR España - (1.95km)
     * -34.905959, -56.163483   Av Brasil y Br Artigas - (2.07km)
     * -34.908898, -56.154578   Av Brasil y Libertad - (1.43km)
     * -34.897494, -56.124601   Rivera y Batlle Ordeñez - (3.57km)
     * -34.881812, -56.093316   Parque Rivera - (6.84km)
     * -34.894080, -56.153162   Estadio Centenario - (3.04km)
     * -34.891106, -56.186442   Palacio Legislativo - (4.68km)
     * -34.869806, -56.167603   Cuernos - (5.92km)
     * -34.902616, -56.124516   Cementerio Buceo - (3.17km)
     * -34.881847, -56.079969   Portones Shopping - (7.81km)
     */

    private User createRootUser(){
        CreateUserRequestDTO requestDTO = new CreateUserRequestDTO();
        ExternalUserDTO externalUserDTO = new ExternalUserDTO();
        externalUserDTO.setName("Agustin");
        externalUserDTO.setLastName("Sarasua");
        externalUserDTO.setEmail("agustinsarasua@gmail.com");
        PasswordRequestDTO passwordRequestDTO = new PasswordRequestDTO();
        passwordRequestDTO.setPassword("Password");
        requestDTO.setUser(externalUserDTO);
        requestDTO.setPassword(passwordRequestDTO);
        AuthenticatedUserTokenDTO token = userService.createUser(requestDTO, Role.AUTHENTICATED);
        String userId = token.getUserId();
        userEntityRepository.save(new UserEntityBuilder(userId).location(-34.921373, -56.150737).age(18).sex("M").name("Agustin").build());
        User user = userDAO.loadByProperty("uuid", userId);
        this.createRootUserProfile(user);
        return user;
    }

    private void createRandomUsers(){
        userEntities.stream().forEach(userEntity -> {
            CreateUserRequestDTO requestDTO = new CreateUserRequestDTO();
            ExternalUserDTO externalUserDTO = new ExternalUserDTO();
            externalUserDTO.setName(userEntity.getName());
            externalUserDTO.setLastName("LastName" + System.currentTimeMillis());
            externalUserDTO.setEmail(System.currentTimeMillis() + "@gmail.com");
            PasswordRequestDTO passwordRequestDTO = new PasswordRequestDTO();
            passwordRequestDTO.setPassword(System.currentTimeMillis() + "");
            requestDTO.setUser(externalUserDTO);
            requestDTO.setPassword(passwordRequestDTO);
            AuthenticatedUserTokenDTO token = userService.createUser(requestDTO, Role.AUTHENTICATED);
            String userId = token.getUserId();
            userEntity.setId(userId);
            userEntityRepository.save(userEntity);
        });

    }

    private void createRootUserProfile(User user){
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setName("Profile Name");
        profileDTO.setSex("M");
        profileDTO.setDiscovery(true);
        profileDTO.setDiscoverySex("F");
        profileDTO.setMinAge(18);
        profileDTO.setMaxAge(28);
        profileService.createProfile(user, profileDTO);
    }

    private void createRootUserFavouriteUsers(User user){
        userEntities.subList(0, userEntities.size() - (userEntities.size() / 2))
                .stream()
        .forEach(userEntity -> {
            favouriteUserService.addFavouriteUser(user, userEntity.getId());
        });
    }

    private void createEventTypes(){
        EventTypeDTO eventTypeDTO = new EventTypeDTO();
        eventTypeDTO.setName("FOOTBALL_MATCH");
        eventTypeDTO.setDescription("Se juega se juega");
        eventService.createEventType(eventTypeDTO);

        EventTypeDTO eventTypeDTO2 = new EventTypeDTO();
        eventTypeDTO2.setName("CONCERT");
        eventTypeDTO2.setDescription("Se juega se juega");
        eventService.createEventType(eventTypeDTO2);
    }

    private String generateUUID(){
        return UUID.randomUUID().toString();
    }
}
