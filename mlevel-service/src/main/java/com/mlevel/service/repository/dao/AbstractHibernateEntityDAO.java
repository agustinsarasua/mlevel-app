package com.mlevel.service.repository.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Provides a way to use annotations to load up DAOs.
 * 
 * @param <T>
 *            the type of entity.
 * @param <ID>
 *            identifier of an entity.
 */
public abstract class AbstractHibernateEntityDAO<T, ID extends Serializable> extends AbstractHibernateDAO {

    protected Class<T> clazz;

    @PostConstruct
    @SuppressWarnings("unchecked")
    protected void loadGenericClass() {
        ParameterizedType parameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();
        this.clazz = ((Class<T>) parameterizedType.getActualTypeArguments()[0]);
    }

    protected Class<T> getClazz() {
        return this.clazz;
    }

    protected DetachedCriteria createCriteria(String classAlias) {
        return DetachedCriteria.forClass(this.clazz, classAlias == null ? this.clazz.getSimpleName().toLowerCase()
                : classAlias);
    }

    protected DetachedCriteria createCriteria() {
        return this.createCriteria(null);
    }

    @SuppressWarnings("unchecked")
    public T read(ID id) {
        return (T) this.getCurrentSession().get(this.clazz, id);
    }

    @SuppressWarnings("unchecked")
    public List<T> readAll() {
        return this.getCurrentSession().createCriteria(this.clazz).list();
    }

    public T load(ID id) {
        T entity = this.read(id);
        if (entity == null) {
            throw new javax.persistence.EntityNotFoundException("Unable to find " + this.clazz.getSimpleName()
                    + " with id #" + id);
        }
        return entity;
    }

    @SuppressWarnings("unchecked")
    public List<T> findByProperty(String property, Object value) {
        return this.getCurrentSession().createCriteria(this.clazz).add(Restrictions.eq(property, value)).list();
    }

    @SuppressWarnings("unchecked")
    public List<T> findByProperty(String property, Object value, int limit) {
        return this.getCurrentSession().createCriteria(this.clazz).setMaxResults(limit).add(Restrictions.eq(property, value)).list();
    }

    @SuppressWarnings("unchecked")
    public List<T> findByProperties(Map<String, Object> properties) {
        Criteria criteria = this.getCurrentSession().createCriteria(this.clazz);
        for (Entry<String, Object> property : properties.entrySet()) {
            criteria = criteria.add(Restrictions.eq(property.getKey(), property.getValue()));
        }
        return criteria.list();
    }

    public T readByProperty(String property, Object value) {
        List<T> result = this.findByProperty(property, value);
        return result.isEmpty() ? null : result.get(0);
    }

    public T loadByProperty(String property, Object value) {
        List<T> result = this.findByProperty(property, value);
        this.validateUnique(result);
        return result.isEmpty() ? null : result.get(0);
    }

    public T readByProperties(Map<String, Object> properties) {
        List<T> result = this.findByProperties(properties);
        this.validateUnique(result);
        return result.isEmpty() ? null : result.get(0);
    }

    public T loadByProperties(Map<String, Object> properties) {
        List<T> result = this.findByProperties(properties);
        if (result.isEmpty()) {
            StringBuilder sb = new StringBuilder("Unable to find ");
            sb.append(this.clazz.getSimpleName()).append("with properties: ");
            for (Entry<String, Object> property : properties.entrySet()) {
                sb.append(" - ").append(property.getKey()).append(": ").append(properties.get(property.getValue()));
            }
            throw new javax.persistence.EntityNotFoundException(sb.toString());
        }
        this.validateUnique(result);
        return result.get(0);
    }

    @SuppressWarnings("unchecked")
	public T readByNamedQuery(String namedQueryName, Object... params) {
        Query query = this.getCurrentSession().getNamedQuery(namedQueryName);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        List<T> result = query.list();
        return result.isEmpty() ? null : result.get(0);
    }

    @SuppressWarnings("unchecked")
    public List<T> listByNamedQuery(String namedQueryName, Object... params) {
        Query query = this.getCurrentSession().getNamedQuery(namedQueryName);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return query.list();
    }

    @SuppressWarnings("unchecked")
    public List<T> listByNamedQueryLimit(String namedQueryName, int limit, Object... params) {
        Query query = this.getCurrentSession().getNamedQuery(namedQueryName);
        query.setMaxResults(limit);
        for (int i = 0; i < params.length; i++) {
            query.setParameter(i, params[i]);
        }
        return query.list();
    }

    // Validations
    private void validateUnique(List<T> result) {
        if (result.size() > 1) {
            throw new javax.persistence.NonUniqueResultException("Expected single result, but got " + result);
        }
    }

    public void save(T entity) {
        this.getCurrentSession().saveOrUpdate(entity);
    }

    public void update(T entity) {
        this.getCurrentSession().update(entity);
    }

    public void refresh(T entity) {
        this.getCurrentSession().refresh(entity);
    }

    public void flush() {
        this.getCurrentSession().flush();
    }

    public void delete(T entity) {
        this.getCurrentSession().delete(entity);
    }
}
