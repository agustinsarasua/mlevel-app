package com.mlevel.service.repository.dao;

import com.mlevel.service.model.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(propagation = Propagation.MANDATORY)
public class ProfileDAO extends AbstractHibernateEntityDAO<Profile, Long> {
}
