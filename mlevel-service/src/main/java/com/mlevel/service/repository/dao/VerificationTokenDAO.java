package com.mlevel.service.repository.dao;

import com.mlevel.service.model.authentication.VerificationToken;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(propagation = Propagation.MANDATORY)
public class VerificationTokenDAO extends AbstractHibernateEntityDAO<VerificationToken, Long> {
}
