package com.mlevel.service.repository.dao.social;

import com.mlevel.service.business.UserService;
import com.mlevel.service.model.User;
import com.mlevel.service.model.authentication.Role;
import com.mlevel.service.model.authentication.SocialUser;
import com.mlevel.service.repository.dao.SocialUserDAO;
import com.mlevel.service.repository.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.social.connect.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class UsersConnectionRepositoryImpl implements UsersConnectionRepository {

    @Autowired
	private SocialUserDAO socialUserDAO;

    @Autowired
    private UserService userService;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private TextEncryptor textEncryptor;

    @Autowired
    private ConnectionFactoryLocator connectionFactoryLocator;

	public UsersConnectionRepositoryImpl() {
	}

    /**
     * Find User with the Connection profile (providerId and providerUserId)
     * If this is the first connection attempt there will be nor User so create one and
     * persist the Connection information
     * In reality there will only be one User associated with the Connection
     *
     * @param connection
     * @return List of User Ids (see User.getUuid())
     */
	public List<String> findUserIdsWithConnection(Connection<?> connection) {
        List<String> userIds = new ArrayList<String>();
        ConnectionKey key = connection.getKey();
        List<SocialUser> users = this.socialUserDAO.listByNamedQuery("SocialUser.findByProviderIdAndProviderUserId", key.getProviderUserId(), key.getProviderId());
        if (!users.isEmpty()) {
            for (SocialUser user : users) {
                userIds.add(user.getUser().getUuid().toString());
            }
            return userIds;
        }
        //First time connected so create a User account or find one that is already created with the email address
        User user = findUserFromSocialProfile(connection);
        String userId;
        if(user == null) {
          userId = userService.createUser(Role.AUTHENTICATED).getUserId();
        } else {
           userId = user.getUuid().toString();
        }
        //persist the Connection
        createConnectionRepository(userId).addConnection(connection);
        userIds.add(userId);

        return userIds;
	}

	public Set<String> findUserIdsConnectedTo(String providerId, Set<String> providerUserIds) {
        List<SocialUser> users = this.socialUserDAO.listByNamedQuery("SocialUser.findByProviderIdAndProviderUserId", providerId, providerUserIds);
        Set<String> result = new HashSet<>();
        users.forEach(socialUser1 -> result.add(socialUser1.getUuid().toString()));
        return result;
	}

	public ConnectionRepository createConnectionRepository(String userId) {
		if (userId == null) {
			throw new IllegalArgumentException("userId cannot be null");
		}
        User user = this.userDAO.readByNamedQuery("User.findByUuid", userId);
        if(user == null) {
            throw new IllegalArgumentException("User not Found");
        }
		return new ConnectionRepositoryImpl(socialUserDAO,userDAO,user, connectionFactoryLocator, textEncryptor);
	}

    private User findUserFromSocialProfile(Connection connection) {
        User user = null;
        UserProfile profile = connection.fetchUserProfile();
        if(profile != null && StringUtils.hasText(profile.getEmail())) {
            user = this.userDAO.readByNamedQuery("User.findByEmail", profile.getEmail());
        }
        return user;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
