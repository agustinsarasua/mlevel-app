package com.mlevel.service.context.config;

import com.mlevel.service.context.util.PropertyConfigurerDescriptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class ServicePropertiesConfig {

    @Bean(name = "service.properties")
    public PropertyConfigurerDescriptor getProperties() {
        PropertyConfigurerDescriptor properties = new PropertyConfigurerDescriptor();
        properties.setApplicationResource(new ClassPathResource("service_des.properties"));
        return properties;
    }

}
