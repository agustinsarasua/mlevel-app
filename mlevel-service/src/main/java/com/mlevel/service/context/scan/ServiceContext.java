package com.mlevel.service.context.scan;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;

import java.util.Properties;

@Configuration
@ComponentScan({"com.mlevel.service"})
public class ServiceContext {

    @Bean(name = "service.properties")
    public PropertiesFactoryBean mapper() {
        PropertiesFactoryBean bean = new PropertiesFactoryBean();
        bean.setLocation(new ClassPathResource("service_des.properties"));
        return bean;
    }

    @Bean(name="javaMailSender")
    public JavaMailSender getJavaMailSender(){
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setPort(587);
        //javaMailSender.setHost("smtp.gmail.com");
        javaMailSender.setHost("smtp.gmail.com");
        javaMailSender.setProtocol("smtp");
        javaMailSender.setUsername("agustinsarasua@gmail.com");
        javaMailSender.setPassword("Agustin9032!");
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        //properties.put("mail.smtp.quitwait",false);
        javaMailSender.setJavaMailProperties(properties);
        return javaMailSender;
    }

    @Bean
    public VelocityEngine getVelocityEngine(){
        VelocityEngine velocityEngine = new VelocityEngine();
        return velocityEngine;
    }

    @Bean
    public TextEncryptor textEncryptor() {
        return Encryptors.noOpText();
    }
}
