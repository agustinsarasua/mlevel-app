package com.mlevel.service.context.config;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.node.NodeClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

import static org.elasticsearch.node.NodeBuilder.nodeBuilder;

@Configuration
@PropertySource("classpath:service_des.properties")
@EnableElasticsearchRepositories(basePackages = "com.mlevel.service.elastic")
public class ElasticsearchConfig {

    @Value("${esearch.port}") int port;
    @Value("${esearch.host}") String hostname;

    @Bean(name = "elasticsearchTemplate")
    public ElasticsearchTemplate elasticsearchTemplate() throws UnknownHostException {
        return new ElasticsearchTemplate(localClient());
        //return new ElasticsearchTemplate(getEmbeddedNodeClient());
    }

    //Embedded Elasticsearch
    private static NodeClient getEmbeddedNodeClient() {
        return (NodeClient) nodeBuilder().clusterName(UUID.randomUUID().toString()).local(true).node()
                .client();
    }

    @Bean
    public Client localClient() throws UnknownHostException {
        TransportClient.Builder builder = TransportClient.builder();
        Client client = builder.build()
                .addTransportAddress(
                    new InetSocketTransportAddress(InetAddress.getByName(this.hostname), this.port));
        return client;
    }

}
