package com.mlevel.service.datastore;

import com.google.gcloud.datastore.*;
import com.mlevel.service.datastore.model.InvitationEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InvitationEntityDatastore extends AbstractDatastore<InvitationEntity> {

    public void updateInvitationState(InvitationEntity.InvitationState state, String invitationId){
        DatastoreOptions options = datastoreHelper.getDatastoreOptions();
        Datastore datastore = options.service();
        KeyFactory keyFactory = datastore.newKeyFactory().kind(InvitationEntity.DS_KIND);
        Key key = keyFactory.newKey(invitationId);
        Entity task = Entity.builder(datastore.get(key)).set(InvitationEntity.PROP_STATE, state.name()).build();
        datastore.update(task);
    }

    public void save(InvitationEntity invitationEntity){
        DatastoreOptions options = datastoreHelper.getDatastoreOptions();
        Datastore datastore = options.service();
        KeyFactory keyFactory = datastore.newKeyFactory().kind(InvitationEntity.DS_KIND);
        Key key = keyFactory.newKey(invitationEntity.getUuid());
        Entity entity = Entity.builder(key)
                .set(InvitationEntity.PROP_ORIGINATOR_UUID, invitationEntity.getOriginatorUUID())
                .set(InvitationEntity.PROP_RECEIVER_UUID, invitationEntity.getReceiverUUID())
                .set(InvitationEntity.PROP_TIMESTAMP, DateTime.now())
                .set(InvitationEntity.PROP_ACTION_NAME, invitationEntity.getActionName())
                .set(InvitationEntity.PROP_ACTION_IMG_URL, invitationEntity.getActionImgUrl())
                .set(InvitationEntity.PROP_DUE_DATE, invitationEntity.getDueDate())
                .set(InvitationEntity.PROP_EVENT_ID, StringUtils.isNotEmpty(invitationEntity.getEventId()) ? invitationEntity.getEventId() : "")
                .set(InvitationEntity.PROP_STATE, invitationEntity.getState().name())
                .set(InvitationEntity.PROP_LAST_UPDATE, DateTime.now()).build();
        datastore.put(entity);
    }

    public List<InvitationEntity> loadInvitationsForUser(String receiverUserUUID) {
        List<InvitationEntity> result = new ArrayList<>();
        DatastoreOptions options = datastoreHelper.getDatastoreOptions();
        Datastore datastore = options.service();
        // Run a query
        Query<Entity> query = Query.entityQueryBuilder()
                .kind(InvitationEntity.DS_KIND)
                .filter(StructuredQuery.PropertyFilter.eq(InvitationEntity.PROP_RECEIVER_UUID, receiverUserUUID))
                .filter(StructuredQuery.PropertyFilter.eq(InvitationEntity.PROP_STATE, InvitationEntity.InvitationState.ACTIVE.name()))
                .build();
        QueryResults<Entity> results = datastore.run(query);
        results.forEachRemaining(entity -> result.add(this.mapEntity(entity)));
        return result;
    }

    public List<InvitationEntity> loadInvitationsFromUser(String originatorUserUUID) {
        List<InvitationEntity> result = new ArrayList<>();
        DatastoreOptions options = datastoreHelper.getDatastoreOptions();
        Datastore datastore = options.service();
        // Run a query
        Query<Entity> query = Query.entityQueryBuilder()
                .kind(InvitationEntity.DS_KIND)
                .filter(StructuredQuery.PropertyFilter.eq(InvitationEntity.PROP_ORIGINATOR_UUID, originatorUserUUID))
                .build();
        QueryResults<Entity> results = datastore.run(query);
        results.forEachRemaining(entity -> result.add(this.mapEntity(entity)));
        return result;
    }

    private InvitationEntity mapEntity(Entity currentEntity){
        InvitationEntity invitationEntity = new InvitationEntity();
        invitationEntity.setUuid(currentEntity.key().toString());
        invitationEntity.setOriginatorUUID(currentEntity.getString(InvitationEntity.PROP_ORIGINATOR_UUID));
        invitationEntity.setReceiverUUID(currentEntity.getString(InvitationEntity.PROP_RECEIVER_UUID));
        invitationEntity.setTimestamp(currentEntity.getDateTime(InvitationEntity.PROP_TIMESTAMP));
        invitationEntity.setActionName(currentEntity.getString(InvitationEntity.PROP_ACTION_NAME));
        invitationEntity.setActionImgUrl(currentEntity.getString(InvitationEntity.PROP_ACTION_IMG_URL));
        invitationEntity.setDueDate(currentEntity.getDateTime(InvitationEntity.PROP_DUE_DATE));
        invitationEntity.setEventId(currentEntity.getString(InvitationEntity.PROP_EVENT_ID));
        invitationEntity.setLastUpdate(currentEntity.getDateTime(InvitationEntity.PROP_LAST_UPDATE));
        invitationEntity.setState(InvitationEntity.InvitationState.valueOf(currentEntity.getString(InvitationEntity.PROP_STATE)));
        return invitationEntity;
    }

}
