package com.mlevel.service.datastore;

import com.google.gcloud.datastore.Datastore;
import com.google.gcloud.datastore.DatastoreOptions;
import org.springframework.stereotype.Component;

@Component
public class DatastoreHelper {

    protected final String PROJECT_ID = "mlevel-app";
    protected final String NAMESPACE = "mlevel-app";
    protected final String HOST = "http://localhost:8247";

    private DatastoreOptions options = null;
    private Datastore datastore = null;

    public DatastoreOptions getDatastoreOptions(){
        if(this.options == null){
            options = DatastoreOptions.builder()
                    .projectId(PROJECT_ID)
                    .namespace(NAMESPACE)
                    .host(HOST)
                    .build();
        }
        return options;
    }

    public Datastore getDatastore(){
        if(this.datastore == null){
            options = DatastoreOptions.builder()
                    .projectId(PROJECT_ID)
                    .namespace(NAMESPACE)
                    .host(HOST)
                    .build();
            datastore = options.service();
        }
        return datastore;
    }
}
