package com.mlevel.service.datastore.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

/**
 * Created by macuco on 17/4/16.
 */
public class ActionEntity implements Serializable {

    private String name;

    private String description;

    public ActionEntity() {
    }

    public ActionEntity(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
