package com.mlevel.service.datastore;

import com.mlevel.service.datastore.model.AbstractDatastoreEntity;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by agustin on 26/02/16.
 */
public abstract class AbstractDatastore <T extends AbstractDatastoreEntity>{

    @Autowired
    protected DatastoreHelper datastoreHelper;
}
