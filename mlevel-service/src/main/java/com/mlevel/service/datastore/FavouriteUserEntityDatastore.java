package com.mlevel.service.datastore;


import com.google.gcloud.datastore.*;
import com.mlevel.service.datastore.model.FavouriteUserEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@Service
public class FavouriteUserEntityDatastore extends AbstractDatastore<FavouriteUserEntity> {

    public void save(FavouriteUserEntity favouriteUserEntity){
        DatastoreOptions options = datastoreHelper.getDatastoreOptions();
        Datastore datastore = options.service();
        KeyFactory keyFactory = datastore.newKeyFactory().kind(FavouriteUserEntity.DS_KIND);
        Key key = keyFactory.newKey(favouriteUserEntity.getUuid());
        Entity entity = Entity.builder(key)
                .set(FavouriteUserEntity.PROP_OWNER_UUID, favouriteUserEntity.getOwnerUUID())
                .set(FavouriteUserEntity.PROP_USER_ADDED_UUID, favouriteUserEntity.getUserAddedUUID())
                .set(FavouriteUserEntity.PROP_TIMESTAMP, DateTime.now())
                .build();
        datastore.put(entity);
    }

    public void delete(FavouriteUserEntity favouriteUserEntity){
        DatastoreOptions options = datastoreHelper.getDatastoreOptions();
        Datastore datastore = options.service();
        KeyFactory keyFactory = datastore.newKeyFactory().kind(FavouriteUserEntity.DS_KIND);
        Key key = keyFactory.newKey(favouriteUserEntity.getUuid());
        datastore.delete(key);
    }


    public FavouriteUserEntity loadByUserAddedUUID(String userAddedUUID){
        List<FavouriteUserEntity> result = new ArrayList<>();
        DatastoreOptions options = datastoreHelper.getDatastoreOptions();
        Datastore datastore = options.service();
        // Run a query
        Query<Entity> query = Query.entityQueryBuilder()
                .kind(FavouriteUserEntity.DS_KIND)
                .filter(StructuredQuery.PropertyFilter.eq(FavouriteUserEntity.PROP_USER_ADDED_UUID, userAddedUUID))
                .build();
        QueryResults<Entity> results = datastore.run(query);
        results.forEachRemaining(entity -> result.add(this.mapEntity(entity)));
        Assert.isTrue(result.size() < 2, "Can not have a favourite user added mora than one time");
        return result.size() == 1 ? result.get(0) : null;
    }

    public FavouriteUserEntity load(String ownerUserUUID, String userAddedUUID){
        List<FavouriteUserEntity> result = new ArrayList<>();
        DatastoreOptions options = datastoreHelper.getDatastoreOptions();
        Datastore datastore = options.service();
        // Run a query
        Query<Entity> query = Query.entityQueryBuilder()
                .kind(FavouriteUserEntity.DS_KIND)
                .filter(StructuredQuery.PropertyFilter.eq(FavouriteUserEntity.PROP_USER_ADDED_UUID, userAddedUUID))
                .filter(StructuredQuery.PropertyFilter.eq(FavouriteUserEntity.PROP_OWNER_UUID, ownerUserUUID))
                .build();
        QueryResults<Entity> results = datastore.run(query);
        results.forEachRemaining(entity -> result.add(this.mapEntity(entity)));
        Assert.isTrue(result.size() < 2, "Can not have a favourite user added mora than one time");
        return result.size() == 1 ? result.get(0) : null;
    }

    public List<FavouriteUserEntity> loadFavourites(String userUUID) {
        List<FavouriteUserEntity> result = new ArrayList<>();
        DatastoreOptions options = datastoreHelper.getDatastoreOptions();
        Datastore datastore = options.service();
        // Run a query
        Query<Entity> query = Query.entityQueryBuilder()
                .kind(FavouriteUserEntity.DS_KIND)
                .filter(StructuredQuery.PropertyFilter.eq(FavouriteUserEntity.PROP_OWNER_UUID, userUUID))
                .build();
        QueryResults<Entity> results = datastore.run(query);
        results.forEachRemaining(entity -> result.add(this.mapEntity(entity)));
        return result;
    }

    private FavouriteUserEntity mapEntity(Entity currentEntity){
        FavouriteUserEntity favUser = new FavouriteUserEntity();
        favUser.setUuid(currentEntity.key().toString());
        favUser.setOwnerUUID(currentEntity.getString(FavouriteUserEntity.PROP_OWNER_UUID));
        favUser.setUserAddedUUID(currentEntity.getString(FavouriteUserEntity.PROP_USER_ADDED_UUID));
        favUser.setTimestamp(currentEntity.getDateTime(FavouriteUserEntity.PROP_TIMESTAMP));
        return favUser;
    }
}
