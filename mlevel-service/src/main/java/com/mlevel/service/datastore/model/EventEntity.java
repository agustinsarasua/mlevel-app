package com.mlevel.service.datastore.model;

import com.google.gcloud.datastore.DateTime;
import com.mlevel.api.ActionDTO;
import com.mlevel.api.location.PlaceDTO;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.util.Date;
import java.util.List;

/**
 * Created by macuco on 12/4/16.
 */
public class EventEntity extends AbstractDatastoreEntity{

    public final static String DS_KIND = "_DS_EVENT_KIND";

    public final static String PROP_UUID = "uuid";
    public final static String PROP_NAME = "name";
    public final static String PROP_SUBTITLE = "subtitle";
    public final static String PROP_DESCRIPTION = "description";
    public final static String PROP_PLACE = "place";
    public final static String PROP_START_DATE = "startTime";
    public final static String PROP_END_DATE = "endTime";
    public final static String PROP_CREATION_DATE = "creationDate";
    public final static String PROP_TYPE = "TYPE";
    public final static String PROP_IMAGE_URL = "imageUrl";
    public final static String PROP_ACTIONS = "actions";

    private String name;

    private String subtitle;

    private String description;

    private DateTime startTime;

    private DateTime endTime;

    private PlaceEntity place;

    private DateTime creationDate;

    private String imageUrl;

    private String type;

    private String timelineItemType;

    private List<ActionEntity> commonActions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    public DateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(DateTime endTime) {
        this.endTime = endTime;
    }

    public PlaceEntity getPlace() {
        return place;
    }

    public void setPlace(PlaceEntity place) {
        this.place = place;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(DateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ActionEntity> getCommonActions() {
        return commonActions;
    }

    public void setCommonActions(List<ActionEntity> commonActions) {
        this.commonActions = commonActions;
    }

    public String getTimelineItemType() {
        return timelineItemType;
    }

    public void setTimelineItemType(String timelineItemType) {
        this.timelineItemType = timelineItemType;
    }

    @Override
    public String getDatastoreKind() {
        return DS_KIND;
    }
}
