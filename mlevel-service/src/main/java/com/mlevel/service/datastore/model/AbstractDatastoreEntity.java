package com.mlevel.service.datastore.model;

import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by agustin on 29/02/16.
 */
public abstract class AbstractDatastoreEntity implements Serializable {

    private String uuid;

    public abstract String getDatastoreKind();

    public AbstractDatastoreEntity() {
        this(UUID.randomUUID());
    }

    public AbstractDatastoreEntity(UUID guid) {
        Assert.notNull(guid, "UUID is required");
        setUuid(guid.toString());
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
