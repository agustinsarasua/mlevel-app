package com.mlevel.service.datastore.model;

import java.io.Serializable;

/**
 * Created by macuco on 23/4/16.
 */
public class LocationEntity implements Serializable {

    private double latitude;

    private double longitude;

    private String city;

    private String country;

    private String address;

    public LocationEntity() {
    }

    public LocationEntity(String address, double latitude, double longitude, String city, String country) {
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
