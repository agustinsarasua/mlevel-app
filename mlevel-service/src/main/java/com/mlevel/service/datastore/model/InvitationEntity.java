package com.mlevel.service.datastore.model;

import com.google.gcloud.datastore.DateTime;

/**
 * Created by agustin on 16/03/16.
 */
public class InvitationEntity extends AbstractDatastoreEntity {

    public final static String DS_KIND = "_DS_INVITATION_ENTITY_KIND";

    public final static String PROP_ORIGINATOR_UUID="originatorUUID";
    public final static String PROP_RECEIVER_UUID="receiverUUID";
    public final static String PROP_TIMESTAMP="timestamp";
    public final static String PROP_ACTION_NAME="actionName";
    public final static String PROP_ACTION_IMG_URL="actionImgUrl";
    public final static String PROP_EVENT_ID="eventId";
    public final static String PROP_LAST_UPDATE="lastUpdate";
    public final static String PROP_DUE_DATE="dueDate";
    public final static String PROP_STATE="state";

    public enum InvitationState {
        ACTIVE, ACCEPTED, DELETED,  CLOSED
    }

    private String originatorUUID;

    private String receiverUUID;

    private DateTime timestamp;

    private String actionName;

    private String actionImgUrl;

    private String eventId;

    private DateTime lastUpdate;

    private DateTime dueDate;

    private InvitationState state;

    public DateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(DateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public InvitationState getState() {
        return state;
    }

    public void setState(InvitationState state) {
        this.state = state;
    }

    public String getOriginatorUUID() {
        return originatorUUID;
    }

    public void setOriginatorUUID(String originatorUUID) {
        this.originatorUUID = originatorUUID;
    }

    public String getReceiverUUID() {
        return receiverUUID;
    }

    public void setReceiverUUID(String receiverUUID) {
        this.receiverUUID = receiverUUID;
    }

    public DateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(DateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionImgUrl() {
        return actionImgUrl;
    }

    public void setActionImgUrl(String actionImgUrl) {
        this.actionImgUrl = actionImgUrl;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public DateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(DateTime dueDate) {
        this.dueDate = dueDate;
    }

    @Override
    public String getDatastoreKind() {
        return DS_KIND;
    }
}
