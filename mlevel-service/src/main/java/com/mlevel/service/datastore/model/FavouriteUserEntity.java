package com.mlevel.service.datastore.model;

import com.google.gcloud.datastore.DateTime;

import java.util.UUID;

/**
 * Created by agustin on 16/03/16.
 */
public class FavouriteUserEntity extends AbstractDatastoreEntity {

    public final static String DS_KIND = "_DS_FAV_USER_ENTITY_KIND";

    public final static String PROP_OWNER_UUID = "ownerUUID";
    public final static String PROP_USER_ADDED_UUID = "userAddedUUID";
    public final static String PROP_TIMESTAMP = "timestamp";
    public final static String PROP_UUID = "uuid";

    private String ownerUUID;

    private String userAddedUUID;

    private DateTime timestamp;

    public String getOwnerUUID() {
        return ownerUUID;
    }

    public void setOwnerUUID(String ownerUUID) {
        this.ownerUUID = ownerUUID;
    }

    public String getUserAddedUUID() {
        return userAddedUUID;
    }

    public void setUserAddedUUID(String userAddedUUID) {
        this.userAddedUUID = userAddedUUID;
    }

    public DateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(DateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getDatastoreKind() {
        return DS_KIND;
    }
}
