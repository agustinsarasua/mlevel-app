package com.mlevel.service.datastore.model;

/**
 * Created by macuco on 23/4/16.
 */
public class PlaceEntity extends AbstractDatastoreEntity {

    public final static String DS_KIND = "_DS_PLACE_ENTITY_KIND";

    private  String facebookId;

    private String name;

    private LocationEntity location;

    public PlaceEntity() {
    }

    public PlaceEntity(LocationEntity location, String facebookId, String name) {
        this.location = location;
        this.facebookId = facebookId;
        this.name = name;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationEntity getLocation() {
        return location;
    }

    public void setLocation(LocationEntity location) {
        this.location = location;
    }

    @Override
    public String getDatastoreKind() {
        return DS_KIND;
    }
}
