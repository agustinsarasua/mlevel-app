package com.mlevel.service.datastore;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gcloud.datastore.*;
import com.mlevel.service.datastore.model.ActionEntity;
import com.mlevel.service.datastore.model.EventEntity;
import com.mlevel.service.datastore.model.PlaceEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by macuco on 17/4/16.
 */
@Service
public class EventEntityDatastore extends AbstractDatastore<EventEntity>{

    private static final Logger LOGGER = LoggerFactory.getLogger(EventEntityDatastore.class);

    @Autowired
    private ObjectMapper mapper;

    public void save(EventEntity eventEntity) {
        DatastoreOptions options = datastoreHelper.getDatastoreOptions();
        Datastore datastore = options.service();
        // Transaction txn = datastore.newTransaction();

        try {
            KeyFactory keyFactory = datastore.newKeyFactory().kind(EventEntity.DS_KIND);
            Key key = keyFactory.newKey(eventEntity.getUuid());

            //  Entity embeddedLocation = Entity.builder(key)
            //  .set(EventEntity.PROP_LOCATION_LAT,eventEntity.getLocation().getLat())
            //  .set(EventEntity.PROP_LOCATION_LON,eventEntity.getLocation().getLon())
            //  .build();

            // FullEntity<IncompleteKey> embeddedLocation = FullEntity.builder()
            // .set(EventEntity.PROP_LOCATION_LAT, eventEntity.getLocation().getLat())
            // .set(EventEntity.PROP_LOCATION_LON, eventEntity.getLocation().getLon())
            // .build();

            String place = this.mapper.writeValueAsString(eventEntity.getPlace());
            String actions = this.mapper.writeValueAsString(eventEntity.getCommonActions());

            /*
            private String name;
            private String subtitle;
            private String description;
            private Date startTime;
            private Date endTime;
            private PlaceEntity place;
            private Date creationDate;
            private String imageUrl;
            private String type;
            private List<ActionEntity> commonActions;
            */

            Entity entity = Entity.builder(key)
                    .set(EventEntity.PROP_UUID, eventEntity.getUuid())
                    .set(EventEntity.PROP_TYPE, eventEntity.getType())
                    .set(EventEntity.PROP_NAME, eventEntity.getName())
                    .set(EventEntity.PROP_SUBTITLE, eventEntity.getSubtitle())
                    .set(EventEntity.PROP_DESCRIPTION, eventEntity.getDescription())
                    .set(EventEntity.PROP_CREATION_DATE, eventEntity.getCreationDate())
                    .set(EventEntity.PROP_START_DATE, eventEntity.getStartTime())
                    .set(EventEntity.PROP_END_DATE, eventEntity.getEndTime())
                    .set(EventEntity.PROP_IMAGE_URL, eventEntity.getImageUrl())
                    .set(EventEntity.PROP_PLACE, place)
                    .set(EventEntity.PROP_ACTIONS, actions)
                    .build();

            datastore.put(entity);
            // txn.put(entity);
            // txn.commit();

        } catch (Exception e){
            LOGGER.error("error saving event on datastore. ", e);
        } finally {
            //if (txn.active()) {
            //    LOGGER.error("rollback operation. ");
            //    txn.rollback();
            //}
        }
    }

    public EventEntity load(String eventId) {
        DatastoreOptions options = datastoreHelper.getDatastoreOptions();
        Datastore datastore = options.service();
        // Run a query
        Query<Entity> query = Query.entityQueryBuilder()
                .kind(EventEntity.DS_KIND)
                .filter(StructuredQuery.PropertyFilter.eq(EventEntity.PROP_UUID, eventId))
                .build();
        Entity entity = datastore.run(query).next();
        return this.mapEntity(entity);
    }

    private EventEntity mapEntity(Entity entity) {
        EventEntity eventEntity = new EventEntity();
        eventEntity.setUuid(entity.getString(EventEntity.PROP_UUID));
        eventEntity.setName(entity.getString(EventEntity.PROP_NAME));
        eventEntity.setSubtitle(entity.getString(EventEntity.PROP_SUBTITLE));
        eventEntity.setDescription(entity.getString(EventEntity.PROP_DESCRIPTION));
        eventEntity.setCreationDate(entity.getDateTime(EventEntity.PROP_CREATION_DATE));
        eventEntity.setStartTime(entity.getDateTime(EventEntity.PROP_START_DATE));
        eventEntity.setEndTime(entity.getDateTime(EventEntity.PROP_END_DATE));
        eventEntity.setType(entity.getString(EventEntity.PROP_TYPE));
        eventEntity.setImageUrl(entity.getString(EventEntity.PROP_IMAGE_URL));
        try {
            eventEntity.setPlace(
                    this.mapper.readValue(
                            entity.getString(EventEntity.PROP_PLACE),
                            new TypeReference<PlaceEntity>() {}));

            eventEntity.setCommonActions(
                    this.mapper.readValue(
                            entity.getString(EventEntity.PROP_ACTIONS),
                            new TypeReference<List<ActionEntity>>() {}));
        } catch (IOException e) {
            LOGGER.error("error on event.commonActions deserialization. ", e);
        }
        return eventEntity;
    }

}
