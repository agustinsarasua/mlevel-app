package com.mlevel.service.model.authentication;

import com.mlevel.service.model.User;
import com.mlevel.service.model.generic.AbstractEntity;

import javax.persistence.*;
@NamedQueries({
        @NamedQuery(name = "SocialUser.findAllByUser", query = "from SocialUser where user.id = ?"),
        @NamedQuery(name = "SocialUser.findByUserAndProviderId", query = "from SocialUser where user.id = ? and providerId = ?"),
        @NamedQuery(name = "SocialUser.findByUserAndProviderUserId", query = "from SocialUser where user.id = ? and providerUserId in (?)"),
        @NamedQuery(name = "SocialUser.findByUserAndProviderIdAndProviderUserId", query = "from SocialUser where user.id = ? and providerUserId = ? and providerId = ?"),
        @NamedQuery(name = "SocialUser.findByProviderIdAndProviderUserId", query = "from SocialUser where providerUserId = ? and providerId = ?")})
@Entity
@Table(name = "SOCIAL_USER")
public class SocialUser extends AbstractEntity{

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "USER_ID", nullable = false, updatable = false)
    private User user;

    @Column(name = "PROVIDER_ID")
    private String providerId;

    @Column(name = "PROVIDER_USER_ID")
    private String providerUserId;

    @Column(name = "RANK")
    private int rank;

    @Column(name = "DISPLAY_NAME")
    private String displayName;

    @Column(name = "PROFILE_URL")
    private String profileUrl;

    @Column(name = "IMAGE_URL")
    private String imageUrl;

    @Column(name = "ACCESS_TOKEN", length = 500)
    private String accessToken;

    @Column(name = "SECRET")
    private String secret;

    @Column(name = "REFRESH_TOKEN")
    private String refreshToken;

    @Column(name = "EXPIRE_TOKEN")
    private Long expireTime;

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderUserId() {
        return providerUserId;
    }

    public void setProviderUserId(String providerUserId) {
        this.providerUserId = providerUserId;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
