package com.mlevel.service.model.authentication;

public enum Role {
    AUTHENTICATED, ADMINISTRATOR, ANONYMOUS
}
