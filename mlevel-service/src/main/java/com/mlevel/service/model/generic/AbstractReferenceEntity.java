package com.mlevel.service.model.generic;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractReferenceEntity extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(name = "CODE", unique = true, nullable = false)
    private String code;

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object obj) {
        // Basic early-cool-checking...
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }

        String otherCode = ((AbstractReferenceEntity) obj).getCode();
        return this.code.equals(otherCode);
    }

    @Override
    public int hashCode() {
        String className = this.getNonProxyClass(this).getName();
        // Uses the "class" hashCode as a super.hashCode...
        return new HashCodeBuilder().appendSuper(className.hashCode()).append(this.getCode()).toHashCode();
    }

}
