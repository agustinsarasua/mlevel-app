package com.mlevel.service.model;

import com.mlevel.service.model.generic.AbstractEntity;

import javax.persistence.*;

@Entity
@Table(name = "PROFILE")
public class Profile extends AbstractEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "IMG_URL")
    private String imgUrl;

    @Column(name = "WELCOME_MESSAGE")
    private String welcomeMessage;

    @Column(name = "DISCOVERY")
    private boolean discovery;

    @Column(name = "SEX")
    private String sex;

    @Column(name = "MIN_AGE")
    private int minAge;

    @Column(name = "MAX_AGE")
    private int maxAge;

    @Column(name = "DISCOVERY_SEX")
    private String discoverySex;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "USER_ID", nullable = false, updatable = false)
    private User user;

    public String getDiscoverySex() {
        return discoverySex;
    }

    public void setDiscoverySex(String discoverySex) {
        this.discoverySex = discoverySex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getWelcomeMessage() {
        return welcomeMessage;
    }

    public void setWelcomeMessage(String welcomeMessage) {
        this.welcomeMessage = welcomeMessage;
    }

    public boolean isDiscovery() {
        return discovery;
    }

    public void setDiscovery(boolean discovery) {
        this.discovery = discovery;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
