package com.mlevel.service.model.generic;

import org.springframework.util.Assert;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractEntity extends AbstractPersistentObject<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "ID", nullable = false)
    private Long id;

    /**
     *  All objects will have a unique UUID which allows for the decoupling from DB generated ids
     *
     */
    @Column(name="UUID", length=36)
    private String uuid;

    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;

    @Column(name="TIME_CREATED")
    private Date timeCreated;

    public AbstractEntity() {
        this(UUID.randomUUID());
    }

    public AbstractEntity(UUID guid) {
        Assert.notNull(guid, "UUID is required");
        setUuid(guid.toString());
        this.timeCreated = new Date();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return this.version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    protected Long identifiedBy() {
        return this.getId();
    }

    public UUID getUuid() {
        return UUID.fromString(uuid);
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(Date timeCreated) {
        this.timeCreated = timeCreated;
    }
}
