package com.mlevel.service.model.authentication;

import com.mlevel.service.model.User;
import com.mlevel.service.model.generic.AbstractEntity;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@NamedQueries({
        @NamedQuery(name = "VerificationToken.findByToken", query = "from VerificationToken where token = ?")})
@Entity
@Table(name = "VERIFICATION_TOKEN")
public class VerificationToken extends AbstractEntity {

    private static final int DEFAULT_EXPIRY_TIME_IN_MINS = 60 * 24; //24 hours

    @Column(name="TOKEN", length=36)
    private final String token;

    @Column(name="EXPIRY_DATE")
    private Date expiryDate;

    @Enumerated(EnumType.STRING)
    @Column(name="TOKEN_TYPE")
    private VerificationTokenType tokenType;

    @Column(name="VERIFIED")
    private boolean verified;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    User user;

    public VerificationToken() {
        super();
        this.token = UUID.randomUUID().toString();
        this.expiryDate = calculateExpiryDate(DEFAULT_EXPIRY_TIME_IN_MINS);
    }

    public VerificationToken(User user, VerificationTokenType tokenType, int expirationTimeInMinutes) {
        this();
        this.user = user;
        this.tokenType = tokenType;
        this.expiryDate = calculateExpiryDate(expirationTimeInMinutes);
    }

    public VerificationTokenType getTokenType() {
        return tokenType;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public String getToken() {
        return token;
    }

    private Date calculateExpiryDate(int expiryTimeInMinutes) {
        DateTime now = new DateTime();
        return now.plusMinutes(expiryTimeInMinutes).toDate();
    }

    public enum VerificationTokenType {
        LOST_PASSWORD, EMAIL_VERIFICATION, EMAIL_REGISTRATION
    }

    public boolean hasExpired() {
        DateTime tokenDate = new DateTime(getExpiryDate());
        return tokenDate.isBeforeNow();
    }

}
