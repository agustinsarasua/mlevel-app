package com.mlevel.service.elastic.repository;

import com.mlevel.service.elastic.UserEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * Created by agustin on 11/03/16.
 */
public interface UserEntityRepository extends ElasticsearchRepository<UserEntity, String> {
    List<UserEntity> findByName(String name);
    List<UserEntity> findByName(String name, Pageable pageable);
    UserEntity findById(String name, String id);
}
