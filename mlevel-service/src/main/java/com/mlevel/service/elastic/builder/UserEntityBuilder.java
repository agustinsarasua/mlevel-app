package com.mlevel.service.elastic.builder;

import com.mlevel.service.elastic.UserEntity;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.data.elasticsearch.core.query.IndexQuery;

import java.util.Date;

/**
 * Created by agustin on 11/03/16.
 */
public class UserEntityBuilder {

    private UserEntity result;

    public UserEntityBuilder(String id) {
        result = new UserEntity();
        result.setId(id);
        result.setTimestamp(new Date());
    }

    public UserEntityBuilder name(String name) {
        result.setName(name);
        return this;
    }

    public UserEntityBuilder sex(String sex) {
        result.setSex(sex);
        return this;
    }

    public UserEntityBuilder imageUrl(String url) {
        result.setImageUrl(url);
        return this;
    }

    public UserEntityBuilder location(double latitude, double longitude) {
        result.setLocation(new GeoPoint(latitude, longitude));
        return this;
    }

    public UserEntityBuilder age(int age) {
        result.setAge(age);
        return this;
    }

    public UserEntity build() {
        return result;
    }

    public IndexQuery buildIndex() {
        IndexQuery indexQuery = new IndexQuery();
        indexQuery.setId(result.getId());
        indexQuery.setObject(result);
        return indexQuery;
    }
}

