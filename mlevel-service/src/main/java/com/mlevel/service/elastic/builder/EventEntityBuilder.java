package com.mlevel.service.elastic.builder;

import com.google.gcloud.datastore.DateTime;
import com.mlevel.api.ActionDTO;
import com.mlevel.api.location.PlaceDTO;
import com.mlevel.service.datastore.model.ActionEntity;
import com.mlevel.service.datastore.model.EventEntity;
import com.mlevel.service.datastore.model.LocationEntity;
import com.mlevel.service.datastore.model.PlaceEntity;
import com.mlevel.service.elastic.LocationEventSearch;
import com.mlevel.service.elastic.SearchActionEntity;
import com.mlevel.service.elastic.SearchEventEntity;
import com.mlevel.service.elastic.SearchPlaceEntity;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by agustin on 16/03/16.
 */
public class EventEntityBuilder {

    // TODO [macuco] mover builder de package elastic?

    private EventEntity event;

    public EventEntityBuilder() {
        event = new EventEntity();
        event.setUuid(UUID.randomUUID().toString());
        event.setCreationDate(DateTime.now());
    }

    public EventEntityBuilder imageUrl (String imageUrl){
        event.setImageUrl(imageUrl);
        return this;
    }

    public EventEntityBuilder type (String type){
        event.setType(type);
        return this;
    }

    public EventEntityBuilder name(String name) {
        event.setName(name);
        return this;
    }

    public EventEntityBuilder subtitle(String subtitle) {
        event.setSubtitle(subtitle);
        return this;
    }

    public EventEntityBuilder description(String description) {
        event.setDescription(description);
        return this;
    }

    public EventEntityBuilder timelineItemType(String timelineItemType) {
        event.setTimelineItemType(timelineItemType);
        return this;
    }

    public EventEntityBuilder startTime(Date startTime) {
        event.setStartTime(DateTime.copyFrom(startTime));
        return this;
    }

    public EventEntityBuilder endTime(Date endTime) {
        event.setEndTime(DateTime.copyFrom(endTime));
        return this;
    }

    public EventEntityBuilder place(PlaceDTO dto) {
        LocationEntity location = new LocationEntity(dto.getLocation().getAddress(),
                dto.getLocation().getLatitude(),dto.getLocation().getLongitude(),dto.getLocation().getCity()
                ,dto.getLocation().getCountry());
        PlaceEntity place = new PlaceEntity(location, dto.getFacebookId(),dto.getName());
        event.setPlace(place);
        return this;
    }

    public EventEntityBuilder actions(List<ActionEntity> actions) {
        event.setCommonActions(actions);
        return this;
    }

    public EventEntityBuilder commonActionsFromDTO(List<ActionDTO> commonActionsDTO) {
        List<ActionEntity> commonActions = commonActionsDTO.stream().map(
                dto -> new ActionEntity(dto.getName(),dto.getDescription()))
                .collect(Collectors.toList());
        event.setCommonActions(commonActions);
        return this;
    }

    public EventEntity build() {
        return event;
    }

    public SearchEventEntity buildIndex() {
        SearchEventEntity searchEvent = new SearchEventEntity();
        searchEvent.setId(event.getUuid());
        searchEvent.setType(event.getType());
        searchEvent.setName(event.getName());
        searchEvent.setSubtitle(event.getSubtitle());
        searchEvent.setDescription(event.getDescription());
        searchEvent.setCreationDate(event.getCreationDate().toDate());
        searchEvent.setStartTime(event.getStartTime().toDate());
        searchEvent.setEndTime(event.getEndTime().toDate());
        searchEvent.setTimelineItemType(event.getTimelineItemType());
        LocationEventSearch location = new LocationEventSearch(
                event.getPlace().getLocation().getLatitude(),
                event.getPlace().getLocation().getLongitude(),
                event.getPlace().getLocation().getCity(),
                event.getPlace().getLocation().getCountry(),
                event.getPlace().getLocation().getAddress());
        searchEvent.setPlace(new SearchPlaceEntity(location,
                event.getPlace().getUuid(),
                event.getPlace().getFacebookId(),
                event.getPlace().getName()));
        searchEvent.setCommonActions(event.getCommonActions().stream().map(ae ->
                new SearchActionEntity(ae.getName(),ae.getDescription()))
                .collect(Collectors.toList()));
        return searchEvent;
    }

    /*
    public IndexQuery buildIndexQuery() {
        IndexQuery indexQuery = new IndexQuery();
        indexQuery.setId(searchEvent.getId());
        indexQuery.setObject(searchEvent);
        return indexQuery;
    }
    */
}
