package com.mlevel.service.elastic;

import org.springframework.data.elasticsearch.core.geo.GeoPoint;

/**
 * Created by macuco on 24/4/16.
 */
public class LocationEventSearch extends GeoPoint {

    private String city;

    private String country;

    private String address;

    public LocationEventSearch(double latitude, double longitude, String city, String country, String address) {
        super(latitude, longitude);
        this.city = city;
        this.country = country;
        this.address = address;
    }

    public LocationEventSearch(double latitude, double longitude) {
        super(latitude, longitude);
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
