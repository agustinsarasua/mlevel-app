package com.mlevel.service.elastic;

import com.mlevel.service.datastore.model.LocationEntity;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

/**
 * Created by macuco on 24/4/16.
 */
public class SearchPlaceEntity {

    private String id;

    private String facebookId;

    private String name;

    private LocationEventSearch location;

    public SearchPlaceEntity(LocationEventSearch location, String id, String facebookId, String name) {
        this.location = location;
        this.id = id;
        this.facebookId = facebookId;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationEventSearch getLocation() {
        return location;
    }

    public void setLocation(LocationEventSearch location) {
        this.location = location;
    }
}
