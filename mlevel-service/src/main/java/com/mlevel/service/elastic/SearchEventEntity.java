package com.mlevel.service.elastic;

import com.mlevel.api.ActionDTO;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.util.Date;
import java.util.List;

@Document(indexName = "event", type = "event")
public class SearchEventEntity extends AbstractElasticEntity {

    public static final String PROP_ID = "id";
    public static final String PROP_EVENT_TYPE = "type";
    public static final String PROP_NAME = "name";
    public static final String PROP_SUBTITLE = "subtitle";
    public static final String PROP_DESCRIPTION = "description";
    public static final String PROP_CREATION_DATE = "creationDate";
    public static final String PROP_START_DATE = "startDate";
    public static final String PROP_END_DATE = "endDate";
    public static final String PROP_TIMELINE_ITEM_TYPE = "timelineItemType";
    public static final String PROP_COMMON_ACTION_NAME = "commonActions.name";
    public static final String PROP_PLACE_NAME = "place.name";
    public static final String PROP_PLACE_COUNTRY = "place.country";
    public static final String PROP_PLACE_CITY = "place.city";
    public static final String PROP_PLACE_LOCATION = "place.location";

    @Id
    private String id;

    private String type;

    private String name;

    private String subtitle;

    private String description;

    private String timelineItemType;

    @Field(type= FieldType.Date)
    private Date creationDate;

    @Field(type= FieldType.Date)
    private Date startTime;

    @Field(type= FieldType.Date)
    private Date endTime;

    @Field(type=FieldType.Nested)
    private SearchPlaceEntity place;

    @Field(type=FieldType.Nested)
    private List<SearchActionEntity> commonActions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public SearchPlaceEntity getPlace() {
        return place;
    }

    public void setPlace(SearchPlaceEntity place) {
        this.place = place;
    }

    public List<SearchActionEntity> getCommonActions() {
        return commonActions;
    }

    public void setCommonActions(List<SearchActionEntity> commonActions) {
        this.commonActions = commonActions;
    }

    public String getTimelineItemType() {
        return timelineItemType;
    }

    public void setTimelineItemType(String timelineItemType) {
        this.timelineItemType = timelineItemType;
    }
}
