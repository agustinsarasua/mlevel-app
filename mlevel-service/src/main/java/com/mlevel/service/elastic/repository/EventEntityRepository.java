package com.mlevel.service.elastic.repository;

import com.mlevel.service.elastic.SearchEventEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Created by agustin on 16/03/16.
 */
public interface EventEntityRepository extends ElasticsearchRepository<SearchEventEntity, String> {
}
