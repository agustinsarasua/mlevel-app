package com.mlevel.service.elastic;

/**
 * Created by macuco on 18/4/16.
 */
public class SearchActionEntity {

        private String name;

        private String description;

        public SearchActionEntity(String name, String description) {
            this.name = name;
            this.description = description;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
