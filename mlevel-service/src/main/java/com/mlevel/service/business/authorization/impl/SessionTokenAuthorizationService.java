package com.mlevel.service.business.authorization.impl;

import com.mlevel.service.business.authorization.AuthorizationRequestContext;
import com.mlevel.service.business.authorization.AuthorizationService;
import com.mlevel.service.business.exception.AuthorizationException;
import com.mlevel.service.model.User;
import com.mlevel.service.model.authentication.AuthorizationToken;
import com.mlevel.service.model.authentication.UserAuthentication;
import com.mlevel.service.repository.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SessionTokenAuthorizationService implements AuthorizationService {

    /**
     * directly access user objects
     */
    @Autowired
    private UserDAO userRepository;

//    public SessionTokenAuthorizationService( repository) {
//        this.userRepository = repository;
//    }

    @Transactional
    public UserAuthentication authorize(AuthorizationRequestContext securityContext) {
        String token = securityContext.getAuthorizationToken();
        UserAuthentication externalUser = null;
        if(token == null) {
            return externalUser;
        }
        User user =  userRepository.readByNamedQuery("User.findBySession", token);
        if(user == null) {
            throw new AuthorizationException();
        }
        AuthorizationToken authorizationToken = user.getAuthorizationToken();
            if (authorizationToken.getToken().equals(token)) {
                externalUser = new UserAuthentication(user);
            }
        return externalUser;
    }
}
