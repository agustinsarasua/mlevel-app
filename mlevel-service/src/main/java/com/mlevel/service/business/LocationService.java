package com.mlevel.service.business;

import com.mlevel.service.elastic.UserEntity;
import com.mlevel.service.elastic.builder.UserEntityBuilder;
import com.mlevel.service.elastic.repository.UserEntityRepository;
import com.mlevel.service.model.User;
import org.joda.time.DateTime;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class LocationService {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private UserEntityRepository userEntityRepository;

    public void updateUserLocation(User user, double latitude, double longitude){
        UserEntity entity = userEntityRepository.findOne(user.getUuid().toString());
        int age = Years.yearsBetween(new DateTime(user.getBirthday()), new DateTime()).getYears();
        if(entity != null){
            entity.setLocation(new GeoPoint(latitude, longitude));
            entity.setTimestamp(new Date());
            userEntityRepository.save(entity);
        } else {
            entity = new UserEntityBuilder(user.getUuid().toString())
                    .location(latitude, longitude)
                    .imageUrl(user.getProfile().getImgUrl())
                    .name(user.getProfile().getName())
                    .age(age)
                    .sex(user.getProfile().getSex())
                    .build();
            userEntityRepository.save(entity);
        }
    }

    public UserEntity loadUserEntity(String uuid){
        return userEntityRepository.findOne(uuid);
    }
}
