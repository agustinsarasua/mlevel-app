package com.mlevel.service.business;

import com.google.gcloud.datastore.DateTime;
import com.mlevel.api.EventDTO;
import com.mlevel.api.EventTypeDTO;
import com.mlevel.service.datastore.EventEntityDatastore;
import com.mlevel.service.datastore.model.EventEntity;
import com.mlevel.service.elastic.SearchEventEntity;
import com.mlevel.service.elastic.builder.EventEntityBuilder;
import com.mlevel.service.elastic.repository.EventEntityRepository;
import com.mlevel.service.model.EventType;
import com.mlevel.service.repository.dao.EventTypeDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
public class EventService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventService.class);

    @Autowired
    private EventEntityRepository eventEntityRepository;

    @Autowired
    private EventTypeDAO eventTypeDAO;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private EventEntityDatastore eventEntityDatastore;

    public String createEvent(EventDTO eventDTO){
        LOGGER.info("validating eventDTO creation");

        Assert.notNull(eventDTO);
        Assert.hasText(eventDTO.getType());
        Assert.hasText(eventDTO.getName());
        Assert.hasText(eventDTO.getSubtitle());
        Assert.hasText(eventDTO.getDescription());
        Assert.hasText(eventDTO.getImageUrl());
        Assert.notNull(eventDTO.getStartTime());
        Assert.notNull(eventDTO.getEndTime());
        Assert.notNull(eventDTO.getTimelineItemType());
        Assert.notNull(eventDTO.getPlace());
        Assert.hasText(eventDTO.getPlace().getName());
        Assert.notNull(eventDTO.getPlace().getLocation());
        Assert.hasText(eventDTO.getPlace().getLocation().getCountry());
        Assert.hasText(eventDTO.getPlace().getLocation().getCity());
        Assert.hasText(eventDTO.getPlace().getLocation().getAddress());
        Assert.notNull(eventDTO.getPlace().getLocation().getLatitude());
        Assert.notNull(eventDTO.getPlace().getLocation().getLongitude());
        Assert.notEmpty(eventDTO.getCommonActions());

        LOGGER.info("Creating New Event");

        EventEntityBuilder eventBuilder = new EventEntityBuilder()
                .type(eventDTO.getType())
                .name(eventDTO.getName())
                .subtitle(eventDTO.getSubtitle())
                .description(eventDTO.getDescription())
                .imageUrl(eventDTO.getImageUrl())
                .startTime(eventDTO.getStartTime())
                .endTime(eventDTO.getEndTime())
                .timelineItemType(eventDTO.getTimelineItemType().name())
                .place(eventDTO.getPlace())
                .commonActionsFromDTO(eventDTO.getCommonActions());

        // save on datastore
        EventEntity eventEntity = eventBuilder.build();
        eventEntityDatastore.save(eventEntity);

        // save on elastic
        SearchEventEntity searchEventEntity = eventBuilder.buildIndex();
        eventEntityRepository.save(searchEventEntity);

        // TODO: [macuco] marcar evento en ds como indexed.

        eventDTO.setUuid(eventEntity.getUuid());
        LOGGER.info("Event [{}] created with id [{}]", eventDTO.getName(), eventDTO.getUuid());

        return eventDTO.getUuid();
    }

    public EventEntity loadEvent(String eventId){
        return eventEntityDatastore.load(eventId);
    }

    @Transactional
    public void createEventType(EventTypeDTO eventTypeDTO){
        Assert.notNull(eventTypeDTO);
        Assert.hasText(eventTypeDTO.getName());

        EventType eventType = new EventType();
        eventType.setName(eventTypeDTO.getName());
        eventType.setActive(true);
        eventType.setDescription(eventTypeDTO.getDescription());
        eventTypeDAO.save(eventType);
    }

    @Transactional
    public List<EventType> loadEventTypes(){
        List<EventType> eventTypes = eventTypeDAO.findByProperty("active", true);
        return eventTypes;
    }

}
