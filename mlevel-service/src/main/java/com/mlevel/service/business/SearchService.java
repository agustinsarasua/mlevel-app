package com.mlevel.service.business;

import com.mlevel.api.location.LocationDTO;
import com.mlevel.service.datastore.model.EventEntity;
import com.mlevel.service.elastic.SearchEventEntity;
import com.mlevel.service.elastic.UserEntity;
import com.mlevel.service.model.Profile;
import com.mlevel.service.model.User;
import org.elasticsearch.index.query.IdsQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SearchService {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private FavouriteUserService favouriteUserService;

    @Autowired
    private EventService eventService;

    public List<UserEntity> loadNearbyUsers(User user, LocationDTO locationDTO){
        Profile userProfile = user.getProfile();
        Assert.notNull(userProfile);
        List<String> favUsersIds = this.favouriteUserService.loadFavouriteUsers(user)
                .stream()
                .map(favouriteUserEntity -> favouriteUserEntity.getUserAddedUUID())
                .collect(Collectors.toList());

        // Favoritos en un radio de 50 km.
        // Desconocidos en un radio de 5km.
        Criteria favUsersCriteria = new Criteria(UserEntity.PROP_LOCATION).within(new GeoPoint(locationDTO.getLatitude(), locationDTO.getLongitude()), "50km")
                .and(UserEntity.PROP_ID).in(favUsersIds);
        // Si tiene activo el discovery obtengo los favoritos y los desconocidos que estén cerca
        // Sino solo los favoritos
        if (userProfile.isDiscovery()) {
            CriteriaQuery criteria = new CriteriaQuery(
                    new Criteria(UserEntity.PROP_SEX)
                            .is(userProfile.getDiscoverySex())
                            .and(UserEntity.PROP_AGE)
                            .between(userProfile.getMinAge(), userProfile.getMaxAge())
                            .and(UserEntity.PROP_LOCATION)
                            .within(new GeoPoint(locationDTO.getLatitude(), locationDTO.getLongitude()), "5km")
                            .or(favUsersCriteria));
            List<UserEntity> result = elasticsearchTemplate.queryForList(criteria, UserEntity.class);
            return result;
        } else {

            CriteriaQuery criteria = new CriteriaQuery(favUsersCriteria);
            List<UserEntity> result = elasticsearchTemplate.queryForList(criteria, UserEntity.class);
            return result;
        }
    }

    public List<EventEntity> loadEventsNear(LocationDTO locationDTO, int kmRadio){
        Assert.notNull(locationDTO);
        Assert.isTrue(kmRadio > 0);
        CriteriaQuery criteria = new CriteriaQuery(
                new Criteria(SearchEventEntity.PROP_PLACE_LOCATION).within(new GeoPoint(locationDTO.getLatitude(), locationDTO.getLongitude()), kmRadio+"km")
                        .and(SearchEventEntity.PROP_START_DATE).greaterThan(new Date()));

        // TODO [macuco] usar queryForIds para no cargar todo el evento desde es
        // SearchQuery searchQuery = new NativeSearchQueryBuilder()
        // .withQuery(queryString(query)).withIndices("event").build();
        // elasticsearchTemplate.queryForIds(searchQuery);

        List<SearchEventEntity> eventIds = elasticsearchTemplate.queryForList(criteria, SearchEventEntity.class);
        return this.loadEventsByIds(eventIds);
    }

    // Buscar evento por texto que esté mas o menos cerca (500km) y que no hayan vencido.
    public List<EventEntity> loadEventsNearByText(String query, LocationDTO locationDTO, int kmRadio){
        Assert.notNull(locationDTO);
        Assert.hasText(query);
        Assert.isTrue(kmRadio > 0);
        CriteriaQuery criteria = new CriteriaQuery(
                new Criteria(SearchEventEntity.PROP_NAME).contains(query)
                        .or(SearchEventEntity.PROP_DESCRIPTION).contains(query)
                        .and(SearchEventEntity.PROP_PLACE_LOCATION).within(new GeoPoint(locationDTO.getLatitude(), locationDTO.getLongitude()), kmRadio+"km")
                        .and(SearchEventEntity.PROP_START_DATE).greaterThan(new Date()));

        // TODO [macuco] usar queryForIds para no cargar todo el evento desde es
        List<SearchEventEntity> eventIds = elasticsearchTemplate.queryForList(criteria, SearchEventEntity.class);
        return this.loadEventsByIds(eventIds);
    }

    private List<EventEntity> loadEventsByIds(List<SearchEventEntity> eventIds){
        return eventIds.stream().map(i -> eventService.loadEvent(i.getId())).collect(Collectors.toList());
    }
}
