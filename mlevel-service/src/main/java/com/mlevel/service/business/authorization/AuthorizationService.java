package com.mlevel.service.business.authorization;

import com.mlevel.service.model.authentication.UserAuthentication;

/**
 *
 * @author: Iain Porter
 */
public interface AuthorizationService {

    /**
     * Given an AuthorizationRequestContext validate and authorize a User
     *
     * @param authorizationRequestContext the context required to authorize a user for a particular request
     * @return ExternalUser
     */
    UserAuthentication authorize(AuthorizationRequestContext authorizationRequestContext);
}
