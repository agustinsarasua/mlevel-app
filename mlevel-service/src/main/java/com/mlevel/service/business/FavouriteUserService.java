package com.mlevel.service.business;

import com.google.gcloud.datastore.DateTime;
import com.mlevel.service.business.exception.BusinessException;
import com.mlevel.service.datastore.FavouriteUserEntityDatastore;
import com.mlevel.service.datastore.model.FavouriteUserEntity;
import com.mlevel.service.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class FavouriteUserService {

    @Autowired
    private FavouriteUserEntityDatastore favouriteUserEntityDatastore;

    public void addFavouriteUser(User currentUser, String userId){
        Assert.notNull(userId);
        FavouriteUserEntity userEntity = favouriteUserEntityDatastore.loadByUserAddedUUID(userId);
        if(userEntity == null) {
            FavouriteUserEntity favouriteUserEntity = new FavouriteUserEntity();
            favouriteUserEntity.setUserAddedUUID(userId);
            favouriteUserEntity.setOwnerUUID(currentUser.getUuid().toString());
            favouriteUserEntity.setTimestamp(DateTime.now());
            favouriteUserEntityDatastore.save(favouriteUserEntity);
        } else {
            throw new BusinessException();
        }
    }

    public void removeFavouriteUser(User currentUser, String userId){
        Assert.notNull(userId);
        FavouriteUserEntity userEntity = favouriteUserEntityDatastore.load(currentUser.getUuid().toString(), userId);
        if(userEntity != null) {
            favouriteUserEntityDatastore.delete(userEntity);
        } else {
            throw new BusinessException();
        }
    }

    public List<FavouriteUserEntity> loadFavouriteUsers(User user){
        Assert.notNull(user);
        return favouriteUserEntityDatastore.loadFavourites(user.getUuid().toString());
    }
}
