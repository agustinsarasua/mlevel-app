package com.mlevel.service.business.authorization.exception;

/**
 *
 * @version 1.0
 * @author: Iain Porter iain.porter@porterhead.com
 * @since 20/10/2012
 */
public class InvalidAuthorizationHeaderException extends RuntimeException {

}
