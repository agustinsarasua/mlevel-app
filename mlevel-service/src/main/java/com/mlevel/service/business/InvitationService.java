package com.mlevel.service.business;

import com.google.gcloud.datastore.DateTime;
import com.mlevel.api.InvitationDTO;
import com.mlevel.service.datastore.InvitationEntityDatastore;
import com.mlevel.service.datastore.model.InvitationEntity;
import com.mlevel.service.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Created by agustin on 17/03/16.
 */
@Service
public class InvitationService {

    @Autowired
    private InvitationEntityDatastore invitationEntityDatastore;

    public void createInvitation(User currentUser, InvitationDTO invitationDTO){
        Assert.notNull(invitationDTO);
        Assert.notNull(currentUser);
        Assert.notNull(invitationDTO.getUserDTO());
        Assert.notNull(invitationDTO.getUserDTO().getUuid());
        Assert.notNull(invitationDTO.getActionDTO());
        Assert.notNull(invitationDTO.getActionDTO().getName());

        InvitationEntity entity = new InvitationEntity();
        entity.setActionName(invitationDTO.getActionDTO().getName());
        // entity.setActionImgUrl(invitationDTO.getActionDTO().getImageUrl());
        entity.setEventId(invitationDTO.getEventDTO() != null ? invitationDTO.getEventDTO().getUuid() : null);
        entity.setDueDate(DateTime.copyFrom(invitationDTO.getInvitationDueDate()));
        entity.setOriginatorUUID(currentUser.getUuid().toString());
        entity.setReceiverUUID(invitationDTO.getUserDTO().getUuid());
        entity.setState(InvitationEntity.InvitationState.ACTIVE);
        invitationEntityDatastore.save(entity);
    }

    public void acceptInvitation(String invitationId) {
        invitationEntityDatastore.updateInvitationState(InvitationEntity.InvitationState.ACCEPTED, invitationId);
    }

    public void rejectInvitation(String invitationId) {
        invitationEntityDatastore.updateInvitationState(InvitationEntity.InvitationState.DELETED, invitationId);
    }

    public List<InvitationEntity> loadInvitationsMadeByMe(User currentUser){
        return invitationEntityDatastore.loadInvitationsFromUser(currentUser.getUuid().toString());
    }

    public List<InvitationEntity> loadInvitationsMadeToMe(User currentUser){
        return invitationEntityDatastore.loadInvitationsForUser(currentUser.getUuid().toString());
    }

}
