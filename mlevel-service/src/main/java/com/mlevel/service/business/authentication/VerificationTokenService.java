package com.mlevel.service.business.authentication;

import com.mlevel.api.authentication.request.LostPasswordRequestDTO;
import com.mlevel.api.authentication.request.PasswordRequestDTO;
import com.mlevel.service.business.EmailService;
import com.mlevel.service.business.exception.*;
import com.mlevel.service.context.config.ApplicationConfig;
import com.mlevel.service.model.authentication.EmailServiceTokenModel;
import com.mlevel.service.model.User;
import com.mlevel.service.model.authentication.Role;
import com.mlevel.service.model.authentication.VerificationToken;
import com.mlevel.service.repository.dao.UserDAO;
import com.mlevel.service.repository.dao.VerificationTokenDAO;
import com.mlevel.service.util.StringUtil;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service
public class VerificationTokenService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private ApplicationConfig config;

    @Autowired
    private EmailService emailService;

    @Autowired
    private VerificationTokenDAO verificationTokenDAO;

    @Transactional
    public VerificationToken verify(String base64EncodedToken) {
        VerificationToken token = loadToken(base64EncodedToken);
        if (token.isVerified() || token.getUser().isVerified()) {
            throw new AlreadyVerifiedException();
        }
        token.setVerified(true);
        token.getUser().setIsVerified(true);
        userDAO.save(token.getUser());
        return token;
    }

    private VerificationToken loadToken(String base64EncodedToken) {
        Assert.notNull(base64EncodedToken);
        String rawToken = new String(Base64.decodeBase64(base64EncodedToken));
        VerificationToken token = verificationTokenDAO.readByNamedQuery("VerificationToken.findByToken",rawToken);
        if (token == null) {
            throw new TokenNotFoundException();
        }
        if (token.hasExpired()) {
            throw new TokenHasExpiredException();
        }
        return token;
    }


    @Transactional
    public VerificationToken sendEmailRegistrationToken(String userId) {
        User user = ensureUserIsLoaded(userId);
        VerificationToken token = new VerificationToken(user,
                VerificationToken.VerificationTokenType.EMAIL_REGISTRATION,
                config.getEmailRegistrationTokenExpiryTimeInMinutes());
        user.addVerificationToken(token);
        userDAO.save(user);
        emailService.sendVerificationEmail(new EmailServiceTokenModel(user,
                token, config.getHostNameUrl()));
        return token;
    }

    private User ensureUserIsLoaded(String userIdentifier) {
        User user;
        if (StringUtil.isValidUuid(userIdentifier)) {
            user = this.userDAO.readByNamedQuery("User.findByUuid", userIdentifier);
        } else {
            user = this.userDAO.readByNamedQuery("User.findByEmail", userIdentifier);
        }
        if (user == null) {
            throw new UserNotFoundException();
        }
        return user;
    }

    @Transactional
    public VerificationToken generateEmailVerificationToken(String emailAddress) {
        Assert.notNull(emailAddress);
        User user = this.userDAO.readByNamedQuery("User.findByEmail", emailAddress);
        if (user == null) {
            throw new UserNotFoundException();
        }
        if (user.isVerified()) {
            throw new AlreadyVerifiedException();
        }
        //if token still active resend that
        VerificationToken token = user.getActiveEmailVerificationToken();
        if (token == null) {
            token = sendEmailVerificationToken(user);
        } else {
            emailService.sendVerificationEmail(new EmailServiceTokenModel(user, token, config.getHostNameUrl()));
        }
        return token;
    }

    private VerificationToken sendEmailVerificationToken(User user) {
        VerificationToken token = new VerificationToken(user, VerificationToken.VerificationTokenType.EMAIL_VERIFICATION,
                config.getEmailVerificationTokenExpiryTimeInMinutes());
        user.addVerificationToken(token);
        userDAO.save(user);
        emailService.sendVerificationEmail(new EmailServiceTokenModel(user, token, config.getHostNameUrl()));
        return token;
    }

    @Transactional
    public VerificationToken sendLostPasswordToken(LostPasswordRequestDTO lostPasswordRequest) {
        VerificationToken token = null;
        User user = this.userDAO.readByNamedQuery("User.findByEmail", lostPasswordRequest.getEmailAddress());
        if (user != null) {
            token = user.getActiveLostPasswordToken();
            if (token == null) {
                token = new VerificationToken(user, VerificationToken.VerificationTokenType.LOST_PASSWORD,
                        config.getLostPasswordTokenExpiryTimeInMinutes());
                user.addVerificationToken(token);
                userDAO.save(user);
            }
            emailService.sendVerificationEmail(new EmailServiceTokenModel(user, token, config.getHostNameUrl()));
        }

        return token;
    }

    @Transactional
    public VerificationToken resetPassword(String base64EncodedToken, PasswordRequestDTO passwordRequest) {
        Assert.notNull(base64EncodedToken);
        VerificationToken token = loadToken(base64EncodedToken);
        if (token.isVerified()) {
            throw new AlreadyVerifiedException();
        }
        token.setVerified(true);
        User user = token.getUser();
        try {
            user.setHashedPassword(user.hashPassword(passwordRequest.getPassword()));
        } catch (Exception e) {
            throw new AuthenticationException();
        }
        //set user to verified if not already and authenticated role
        user.setIsVerified(true);
        if (user.hasRole(Role.ANONYMOUS)) {
            user.setRole(Role.AUTHENTICATED);
        }
        userDAO.save(user);
        return token;
    }
}
