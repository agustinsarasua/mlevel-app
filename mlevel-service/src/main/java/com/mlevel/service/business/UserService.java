package com.mlevel.service.business;

import com.mlevel.api.authentication.AuthenticatedUserTokenDTO;
import com.mlevel.api.authentication.ExternalUserDTO;
import com.mlevel.api.authentication.request.CreateUserRequestDTO;
import com.mlevel.api.authentication.request.LoginRequestDTO;
import com.mlevel.service.business.exception.AuthenticationException;
import com.mlevel.service.business.exception.AuthorizationException;
import com.mlevel.service.business.exception.DuplicateUserException;
import com.mlevel.service.business.exception.UserNotFoundException;
import com.mlevel.service.context.config.ApplicationConfig;
import com.mlevel.service.datastore.DatastoreHelper;
import com.mlevel.service.model.User;
import com.mlevel.service.model.authentication.AuthorizationToken;
import com.mlevel.service.model.authentication.Role;
import com.mlevel.service.repository.dao.UserDAO;
import com.mlevel.service.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserService implements UserDetailsService {

/*
    @Autowired
    private UserDatastore userDatastore;
*/
    @Autowired
    private UserDAO userDAO;



    @Autowired
    private ApplicationConfig applicationConfig;

    @Autowired
    private UsersConnectionRepository usersConnectionRepository;


    @Transactional
    public AuthenticatedUserTokenDTO createUser(CreateUserRequestDTO request, Role role) {
        User users = this.userDAO.readByNamedQuery("User.findByEmail", request.getUser().getEmail());
        if (users != null) {
            throw new DuplicateUserException();
        }
        User newUser = createNewUser(request, role);
        AuthenticatedUserTokenDTO token = new AuthenticatedUserTokenDTO(newUser.getUuid().toString(), createAuthorizationToken(newUser).getToken());
        userDAO.save(newUser);
        return token;
    }

    @Transactional
    public AuthenticatedUserTokenDTO createUser(Role role) {
        User user = new User();
        user.setRole(role);
        AuthenticatedUserTokenDTO token = new AuthenticatedUserTokenDTO(user.getUuid().toString(),
                createAuthorizationToken(user).getToken());
        userDAO.save(user);
        return token;
    }

    private User createNewUser(CreateUserRequestDTO request, Role role) {
        User userToSave = new User();
        userToSave.setName(request.getUser().getName());
        userToSave.setLastName(request.getUser().getLastName());
        userToSave.setEmail(request.getUser().getEmail());
        try {
            userToSave.setHashedPassword(userToSave.hashPassword(request.getPassword().getPassword()));
        }  catch (Exception e) {
            throw new AuthenticationException();
        }
        userToSave.setRole(role);
        return userToSave;
    }

    public AuthorizationToken createAuthorizationToken(User user) {
        if(user.getAuthorizationToken() == null || user.getAuthorizationToken().hasExpired()) {
            user.setAuthorizationToken(new AuthorizationToken(user, applicationConfig.getAuthorizationExpiryTimeInSeconds()));
            userDAO.save(user);
        }
        return user.getAuthorizationToken();
    }

    @Transactional
    public AuthenticatedUserTokenDTO socialLogin(Connection<?> connection) {

        List<String> userUuids = usersConnectionRepository.findUserIdsWithConnection(connection);
        if(userUuids.size() == 0) {
            throw new AuthenticationException();
        }
        User user = this.userDAO.readByNamedQuery("User.findByUuid", userUuids.get(0)); //take the first one if there are multiple userIds for this provider Connection
        if (user == null) {
            throw new AuthenticationException();
        }
        updateUserFromProfile(connection, user);
        return new AuthenticatedUserTokenDTO(user.getUuid().toString(), createAuthorizationToken(user).getToken());
    }

    private void updateUserFromProfile(Connection<?> connection, User user) {
        UserProfile profile = connection.fetchUserProfile();
        user.setEmail(profile.getEmail());
        user.setName(profile.getFirstName());
        user.setLastName(profile.getLastName());
        //users logging in from social network are already verified
        user.setIsVerified(true);
        if(user.hasRole(Role.ANONYMOUS)) {
            user.setRole(Role.AUTHENTICATED);
        }
        userDAO.save(user);
    }

    @Transactional
    public AuthenticatedUserTokenDTO login(LoginRequestDTO request) {
        User user = this.userDAO.readByNamedQuery("User.findByEmail", request.getUsername());
        if (user == null) {
            throw new AuthenticationException();
        }
        String hashedPassword = null;
        try {
            hashedPassword = user.hashPassword(request.getPassword());
        } catch (Exception e) {
            throw new AuthenticationException();
        }
        if (hashedPassword.equals(user.getHashedPassword())) {
            return new AuthenticatedUserTokenDTO(user.getUuid().toString(), createAuthorizationToken(user).getToken());
        } else {
            throw new AuthenticationException();
        }
    }

    @Transactional
    public ExternalUserDTO getUser(ExternalUserDTO requestingUser, String userIdentifier) {
        Assert.notNull(requestingUser);
        Assert.notNull(userIdentifier);
        User user = ensureUserIsLoaded(userIdentifier);
        if(!requestingUser.getId().equals(user.getUuid().toString()) && !requestingUser.getRole().equalsIgnoreCase(Role.ADMINISTRATOR.toString()))  {
            throw new AuthorizationException();
        }
        ExternalUserDTO externalUserDTO = new ExternalUserDTO();
        externalUserDTO.setName(user.getName());
        externalUserDTO.setEmail(user.getEmail());
        return externalUserDTO;
    }

    private User ensureUserIsLoaded(String userIdentifier) {
        User user;
        if (StringUtil.isValidUuid(userIdentifier)) {
            user = this.userDAO.readByNamedQuery("User.findByUuid", userIdentifier);
        } else {
            user = this.userDAO.readByNamedQuery("User.findByEmail", userIdentifier);
        }
        if (user == null) {
            throw new UserNotFoundException();
        }
        return user;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userDAO.readByNamedQuery("User.findByEmail", username);
        return user;
    }

}
