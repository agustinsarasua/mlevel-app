package com.mlevel.service.business;

import com.mlevel.api.ProfileDTO;
import com.mlevel.service.model.Profile;
import com.mlevel.service.model.User;
import com.mlevel.service.repository.dao.ProfileDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service
public class ProfileService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileService.class);

    @Autowired
    private ProfileDAO profileDAO;

    @Transactional
    public void createProfile(User user, ProfileDTO profileDTO){
        LOGGER.info("Updating profile for user {}", user.getName());
        validate(profileDTO);
        Profile profile = new Profile();
        profile.setDiscovery(profileDTO.isDiscovery());
        profile.setDiscoverySex(profileDTO.getDiscoverySex());
        profile.setMaxAge(profileDTO.getMaxAge());
        profile.setMinAge(profileDTO.getMinAge());
        profile.setSex(profileDTO.getSex());
        profile.setWelcomeMessage(profileDTO.getWelcomeMessage());
        profile.setImgUrl(profileDTO.getImgUrl());
        profile.setUser(user);
        user.setProfile(profile);
        profileDAO.save(profile);
    }

    @Transactional
    public void updateProfile(User user, ProfileDTO profileDTO){
        LOGGER.info("Updating profile for user {}", user.getName());
        validate(profileDTO);
        Profile currentProfile = user.getProfile();
        currentProfile.setDiscovery(profileDTO.isDiscovery());
        currentProfile.setDiscoverySex(profileDTO.getDiscoverySex());
        currentProfile.setMaxAge(profileDTO.getMaxAge());
        currentProfile.setMinAge(profileDTO.getMinAge());
        currentProfile.setSex(profileDTO.getSex());
        currentProfile.setWelcomeMessage(profileDTO.getWelcomeMessage());
        currentProfile.setImgUrl(profileDTO.getImgUrl());
        profileDAO.update(currentProfile);
    }

    private void validate(ProfileDTO profileDTO){
        Assert.notNull(profileDTO);
        Assert.hasText(profileDTO.getName());
        Assert.hasText(profileDTO.getSex());
        if(profileDTO.isDiscovery()){
            Assert.hasText(profileDTO.getDiscoverySex());
            Assert.isTrue(profileDTO.getMaxAge() > profileDTO.getMinAge());
        }
    }
}
